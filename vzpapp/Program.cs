﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace vzpapp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        ///  
        static Mutex mutex = new Mutex(false, "483dfd27 - 2aca - 4ac4 - 8bed - e6cbf3980e62");
        static Form SplashScreen;
        //static Form MainForm;
        [STAThread]

       
        static void Main()
        {

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                SplashScreen = new SplashForm();                              
                Application.Run(SplashScreen);
                              
                mutex.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("only one instance at a time");
            }


        }

       
    }

}
