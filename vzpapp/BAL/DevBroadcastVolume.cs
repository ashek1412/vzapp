﻿using System;
using System.Runtime.InteropServices;

namespace vzpapp.BAL
{
    public  class DevBroadcastVolume
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct DevBroadcastVolumes
        {
            public int Size;
            public int DeviceType;
            public int Reserved;
            public int Mask;
            public Int16 Flags;
        }


    }
}
