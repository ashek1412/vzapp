﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace vzpapp.BAL
{
    public class Tickets
    {
        public string id { get; set; }
        public string username { get; set; }
        public string subject { get; set; }
        public string content { get; set; }
        public string type { get; set; }
        public string status { get; set; }
        public string files { get; set; }
        public string sfile { get; set; }

        public string cus_name { get; set; }
        public string cus_license { get; set; }
        public string cus_vin { get; set; }
        public string cus_dtc { get; set; }     
        public string v_producer { get; set; }
        public string v_series { get; set; }
        public string v_build { get; set; }               
        public string ecu_producer { get; set; }
        public string ecu_build { get; set; }   
        public string ecu_file_type { get; set; }






        public DateTime createDate { get; set; }
        public DateTime updateDate { get; set; }
        List<Tickets> DestopTickets;
        MemoryStream responseStream;


        public List<Tickets> GetUserTickets()
        {

            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/get_all_user_tickets");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", BAL.Globals.user_name);


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                if (!String.IsNullOrEmpty(response.Content))
                    DestopTickets = serializer.Deserialize<List<Tickets>>(response.Content);
                else
                    return null;
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                //return usrv.Select(s => s.fid).ToList(); 
                return DestopTickets;
            }
            catch (Exception)
            {
                return null;
            }

        }


        public MemoryStream DownLoadTicket(string ecu_dl_file)
        {
            responseStream = new MemoryStream();
            responseStream = null;
            if (BAL.Globals.CheckInternet() == false)
                return responseStream;
            try
            {


                var client = new RestClient("https://www.ecu-manager.com/magic/api/download_ticket_file");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("fname", ecu_dl_file);

                var encoding = ASCIIEncoding.ASCII;



                responseStream = new MemoryStream();
                Byte[] response = client.DownloadData(request);
                //response.GetResponseStream().CopyTo(responseStream);
                //responseStream.Write(response);
                if (response.Length > 0)
                {
                    responseStream = new MemoryStream(response);

                    return responseStream;
                }
                else
                {
                    return responseStream;

                }





            }
            catch (Exception we)
            {
                //String status = ((FtpWebResponse)e.Response).StatusDescription;
                MessageBox.Show("Cannot Download file !!");
                return responseStream;

                //Globals.WriteToFile(Globals.ExceptionInfo(e));
                //return 0;
            }


        }



    }
}
