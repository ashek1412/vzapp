﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Script.Serialization;

namespace vzpapp.BAL
{
    public class DNews
    {
        public string id { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public string ustat { get; set; }
        public DateTime create_at { get; set; }

        List<DNews> DestopNews;


        public List<DNews> GetUserNews()
        {

            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/get_desktop_news");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", BAL.Globals.user_name);


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                if (!String.IsNullOrEmpty(response.Content))
                    DestopNews = serializer.Deserialize<List<DNews>>(response.Content);
                else
                    return null;
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                //return usrv.Select(s => s.fid).ToList(); 
                return DestopNews;
            }
            catch (Exception)
            {
                return null;
            }

        }



        public void SetNewsViewed(string user_name, string news_id)
        {
            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/set_news_as_viewed");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", user_name);
                request.AddParameter("nid", news_id);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

            }
            catch (Exception ecp)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ecp));


            }


        }
    }
}
