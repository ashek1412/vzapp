﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace vzpapp.BAL
{
    public class Ecu
    {
        public int id { get; set; }
        List<String> Build_grp; 
        public string producer { get; set; }
        public string build_sgrp { get; set; }
        public string use { get; set; }       
        public List<Ecu> ecuList;
        List<String> ecuUse;

        public List<Ecu> GetEcu()
        {
            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/get_ecu_info");
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                if (!String.IsNullOrEmpty(response.Content))
                    ecuList = serializer.Deserialize<List<Ecu>>(response.Content);
                else
                    return null;


                return ecuList;
            }
            catch (Exception )
            {
                return null;
            }


        }


        public List<String> GetEcuUse()
        {
            ecuUse = new List<string>();
            ecuUse.Add("ABS");
            ecuUse.Add("CPC");
            ecuUse.Add("Engine");
            ecuUse.Add("Gear");

            return ecuUse;


        }


        public List<String> GetEcuBuild(string producer, string usecombo)
        {
            try
            {
             

                if (String.IsNullOrEmpty(usecombo))
                {
                    Build_grp = Globals.allECU.OrderBy(x => x.build_sgrp).Where(x => x.producer == producer).Select(x => x.build_sgrp).Distinct().ToList();
                }
                else
                {
                    Build_grp = Globals.allECU.OrderBy(x => x.build_sgrp).Where(x => x.producer == producer || x.use == usecombo).Select(x => x.build_sgrp).Distinct().ToList();
                }
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                Build_grp.RemoveAll(s => string.IsNullOrWhiteSpace(s));
                return Build_grp;
            }
            catch (Exception )
            {
                return null;
            }



        }

    }
}
