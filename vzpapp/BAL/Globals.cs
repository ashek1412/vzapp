﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vzpapp.BAL
{
    public static class Globals
    {

        public static  string user_name { get; set; }
        public static string user_sl { get; set; }
        public static string user_macid { get; set; }
        public static DateTime exp_date { get; set; }
        public static int usbErrCnt;
        public static int news_stat;
        public static int tickets_stat;
        public static int tickets_dl_stat;
        public static string dlticket { get; set; }
        public static string usbDrive { get; set; }
        public static int user_news { get; set; }



        public static List<UserService> user_service;
        public static List<DNews> allNews;
        public static List<Tickets> allTickets;

        public static List<Vehicles> allVehilces;
        public static List<Ecu> allECU;

        public static Reg currentUser;

        


        public static bool CheckInternet()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("https://www.ecu-manager.com"))
                {
                    return true;
                }
            }
            catch (Exception exp)
            {
                //MessageBox.Show(exp.Message);
                return false;
            }
        }



        public static int VerifyUsbDvice()
        {

            int usbstat = 0;


            if (Globals.currentUser == null || String.IsNullOrEmpty(Globals.currentUser.email))
            {
                DialogResult dialog = MessageBox.Show("User Not Found for this Device !!", "Error", MessageBoxButtons.OK);
                if (dialog == DialogResult.OK)
                {
                    Application.Exit();
                    return usbstat;
                }

            }
            else if (String.IsNullOrEmpty(Globals.currentUser.device_pkey) || String.IsNullOrEmpty(Globals.currentUser.device_id))
            {
                DialogResult dialog = MessageBox.Show("USB Device information missing", "Error", MessageBoxButtons.OK);
                if (dialog == DialogResult.OK)
                {
                    Application.Exit();
                    return usbstat;
                }
            }
            else
            {

                usbstat = Globals.currentUser.GetUsbStatus();              

                return usbstat;
            }


            return usbstat;

        }

        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }


        public static string ExceptionInfo(this Exception exception)
        {

            StackFrame stackFrame = (new StackTrace(exception, true)).GetFrame(0);
            return string.Format("At line {0} column {1} in {2}: {3} {4}{3}{5}  ",
               stackFrame.GetFileLineNumber(), stackFrame.GetFileColumnNumber(),
               stackFrame.GetMethod(), Environment.NewLine, stackFrame.GetFileName(),
               exception.Message);

        }



       


    }
}
