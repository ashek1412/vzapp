﻿using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace vzpapp.BAL
{
    

    class EcuFIle
    {

        public string cus_name { get; set; }
        public string cus_license { get; set; }
        public string cus_vin { get; set; }
        public string cus_dtc { get; set; }
        public string desktop_user { get; set; }
        public string v_type { get; set; }
        public string v_producer { get; set; }
        public string v_series { get; set; }
        public string v_build { get; set; }
        public string v_model { get; set; }
        public string v_year { get; set; }
        public string ecu_use { get; set; }
        public string ecu_producer { get; set; }
        public string ecu_build { get; set; }
        public string ecu_nr_prd { get; set; }
        public string ecu_nr_ecu { get; set; }
        public string ecu_software { get; set; }
        public string ecu_version { get; set; }
        public byte[] ecu_file { get; set; }
        public string ecu_file_type { get; set; }
        public string ecu_file_name { get; set; }
        public string ecu_file_path { get; set; }
        public string ecu_dl_file_path { get; set; }
        public string ecu_dl_file { get; set; }
        public int ecu_file_save_id { get; set; }
        public int ecu_file_dl_stat { get; set; }

        class EcuFileRes
        {
            public string stat { get; set; }
            public string file { get; set; }
        }



        public int CheckEcuFile()
        {
           try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/check_ecu_file");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("ecu_file_id",this.ecu_file_save_id);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                EcuFileRes dynObj = serializer.Deserialize<EcuFileRes>(response.Content);
                if (dynObj.stat == "1" && dynObj.file.Length > 0)
                {
                    this.ecu_dl_file_path = "https://www.ecu-manager.com/magic/ecufiles/2/" + this.ecu_file_save_id+"/"+ dynObj.file;
                    this.ecu_dl_file = dynObj.file;
                    return 1;
                }
                else if (dynObj.stat == "3")
                {

                    return 2;
                }
                else
                    return 0;
            }
            catch (Exception )
            {

                return 0;
            }           


        }



        public int CancelEcuFile()
        {
            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/cancel_ecu_file");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("ecu_file_id", this.ecu_file_save_id);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                EcuFileRes dynObj = serializer.Deserialize<EcuFileRes>(response.Content);
                if (dynObj.stat == "1")
                {
                  
                    return 1;
                }               
                else
                    return 0;
            }
            catch (Exception)
            {

                return 0;
            }


        }

        public int CancelEcuFileByUesr()
        {
            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/cancel_ecu_file_user");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("ecu_file_id", this.ecu_file_save_id);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                EcuFileRes dynObj = serializer.Deserialize<EcuFileRes>(response.Content);
                if (dynObj.stat == "1")
                {

                    return 1;
                }
                else
                    return 0;
            }
            catch (Exception)
            {

                return 0;
            }


        }


        public int SaveEcuFile()
        {
            if (BAL.Globals.CheckInternet() == false)

                return -1;

            try
            {


                var client = new RestClient("https://www.ecu-manager.com/magic/api/new_ecu_file");
                var request = new RestRequest(Method.POST);
                request.AlwaysMultipartFormData = true;
                request.AddHeader("Content-Type", "multipart/form-data");
                //request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Accept", "application/json");
                // request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("desktop_user", Globals.user_name); 
                request.AddParameter("cus_name", this.cus_name);
                request.AddParameter("cus_license", this.cus_license);
                request.AddParameter("cus_vin", this.cus_vin);
                request.AddParameter("cus_dtc", this.cus_dtc);
                request.AddParameter("v_type", this.v_type);
                request.AddParameter("v_producer", this.v_producer);
                request.AddParameter("v_series", this.v_series);
                request.AddParameter("v_build", this.v_build);
                request.AddParameter("v_model", this.v_model);
                request.AddParameter("ecu_use", this.ecu_use);
                request.AddParameter("ecu_producer", this.ecu_producer);
                request.AddParameter("ecu_build", this.ecu_build);
                request.AddParameter("ecu_nr_prd", this.ecu_nr_prd);
                request.AddParameter("ecu_nr_ecu", this.ecu_nr_ecu);               
                request.AddParameter("ecu_software", this.ecu_software);
                request.AddParameter("ecu_version", this.ecu_version);
                request.AddFile("ecu_file", this.ecu_file_path);
                request.AddParameter("ecu_file_type", this.ecu_file_type);
                request.AddParameter("entry_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));



                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                //var token = d.token;
                int t =Convert.ToInt32(dynObj.stat.ToString());
                return t;
            }
            catch (Exception nd)
            {
                MessageBox.Show("Unable to Process File");
               
                return -1;
            }

        }


        public Byte[]  PrepareEcuFile(string fullpath)
        {
            try
            {
                string fileName = fullpath;
                byte[] array;
                using (FileStream fs = File.OpenRead(fileName))
                {
                    array = new byte[fs.Length];
                    using (BinaryReader binReader = new BinaryReader(fs))
                    {
                        array = binReader.ReadBytes((int)fs.Length);
                    }
                }

                return array;
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
                return null;
            }


        }


        public string SetDownloadFileName()
        {
            string fname = "EM_";

            if (!String.IsNullOrEmpty(this.ecu_file_type))
                fname = fname + this.ecu_file_type.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.cus_name))
                fname = fname + "_" + this.cus_name.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.cus_license))
                fname = fname + "_" + this.cus_license.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.cus_vin))
                fname = fname + "_" + this.cus_vin.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.cus_dtc))
                fname = fname + "_" + this.cus_dtc.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.v_producer))
                fname = fname + "_" + this.v_producer.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.v_series))
                fname = fname + "_" + this.v_series.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.v_build))
                fname = fname + "_" + this.v_build.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.ecu_producer))
                fname = fname + "_" + this.ecu_producer.Replace(" ", "_");

            if (!String.IsNullOrEmpty(this.ecu_build))
                fname = fname + "_" + this.ecu_build.Replace(" ", "_");

            fname =Regex.Replace(fname, "[^a-zA-Z0-9_]+", "_", RegexOptions.Compiled);

            fname = fname + ".bin";


            return fname;
        }



    }


}
