﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace vzpapp.BAL
{
    public class Vehicles
    {

        public int id { get; set; }
        public string type { get; set; }
        public string producer { get; set; }
        public string series { get; set; }
        public string build { get; set; }
        public string model { get; set; }
        public List<Vehicles> vList;


        public List<Vehicles> GetVehilces()
        {
            try
            {
                var client = new RestClient("https://www.ecu-manager.com/magic/api/get_vehicle_info");
                var request = new RestRequest(Method.GET);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/json");


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if(!String.IsNullOrEmpty(response.Content))
                    vList = serializer.Deserialize<List<Vehicles>>(response.Content);
                else
                    return null;


                return vList;
            }
            catch (Exception)
            {
                return null;
            }


        }
        public List<String> GetVehicleProducer(string vtype)
        {
            try
            {

                var prod = Globals.allVehilces.OrderBy(x => x.producer).Where(x => x.type == vtype).Select(x => x.producer).Distinct().ToList();
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                return prod;
            }
            catch (Exception)
            {
                return null;
            }


        }

        public List<String> GetVehicleSeries(string vtype, string producer)
        {
            try
            {

                var serieses = Globals.allVehilces.OrderBy(x => x.series).Where(x => x.type == vtype).Where(x => x.producer == producer).Select(x => x.series).Distinct().ToList();
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                return serieses;
            }
            catch (Exception )
            {
                return null;
            }


        }


        public List<String> GetVehiclebuild(string vtype, string producer, string series)
        {
            try
            {

                var builds = Globals.allVehilces.OrderBy(x => x.series).Where(x => x.type == vtype).Where(x => x.producer == producer).Where(x => x.series == series).Select(x => x.build).Distinct().ToList();
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                return builds;
            }
            catch (Exception )
            {
                return null;
            }


        }

        public List<String> GetVehicleModel(string vtype, string producer, string series, string build = "")
        {
            try
            {

                var models = Globals.allVehilces.OrderBy(x => x.series).Where(x => x.type == vtype).Where(x => x.producer == producer).Where(x => x.series == series).Select(x => x.model).Distinct().ToList();
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                return models;
            }
            catch (Exception )
            {
                return null;
            }


        }
        public List<String> GetVehicleModelBuild(string vtype, string producer, string series, string build)
        {
            try
            {

                var models = Globals.allVehilces.OrderBy(x => x.series).Where(x => x.type == vtype).Where(x => x.producer == producer).Where(x => x.series == series).Where(x => x.build == build).Select(x => x.model).Distinct().ToList();
                //var token = d.token;
                //String t = dynObj.stat.ToString();
                return models;
            }
            catch (Exception )
            {
                return null;
            }


        }


        public List<int> GetVehicleYear()
        {
            List<int> last20Years = new List<int>();
            int currentYear = DateTime.Now.Year;
            for (int i = currentYear - 20; i <= currentYear; i++)
            {
                last20Years.Add(i);
            }
            last20Years.Add(currentYear+1);

            return last20Years;
        }
    }




}
