﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace vzpapp.BAL
{
    class Crypto
    {
		private static readonly Encoding encoding = Encoding.UTF8;
		//private static readonly String  encoding = Encoding.UTF8;
		

		static RSACryptoServiceProvider rsa = null;
		public static string publicPrivateKeyXML { set; get; }
		public static string publicOnlyKeyXML { set; get; }



		public static void AssignNewKey()
		{
			const int PROVIDER_RSA_FULL = 1;
			const string CONTAINER_NAME = "KeyContainer";
			CspParameters cspParams;
			cspParams = new CspParameters(PROVIDER_RSA_FULL);
			cspParams.KeyContainerName = CONTAINER_NAME;
			cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
			cspParams.ProviderName = "Microsoft Strong Cryptographic Provider";
			rsa = new RSACryptoServiceProvider(cspParams);

			//Pair of public and private key as XML string.
			//Do not share this to other party
			publicPrivateKeyXML = rsa.ToXmlString(true);

			//Private key in xml file, this string should be share to other parties
			//publicOnlyKeyXML = rsa.ToXmlString(false);

		}





		// Create a method to encrypt a text and save it to a specific file using a RSA algorithm public key   
		public static void EncryptText(string publicKey, string text, string fileName)
		{
			// Convert the text to an array of bytes   
			UnicodeEncoding byteConverter = new UnicodeEncoding();
			byte[] dataToEncrypt = byteConverter.GetBytes(text);

			// Create a byte array to store the encrypted data in it   
			byte[] encryptedData;
			using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
			{
				// Set the rsa pulic key   
				rsa.FromXmlString(publicKey);

				// Encrypt the data and store it in the encyptedData Array   
				encryptedData = rsa.Encrypt(dataToEncrypt, false);
			}
			// Save the encypted data array into a file   
			File.WriteAllBytes(fileName, encryptedData);

			//Console.WriteLine("Data has been encrypted");
		}

		// Method to decrypt the data withing a specific file using a RSA algorithm private key   
		public static string DecryptData(string privateKey, string fileName)
		{
			// read the encrypted bytes from the file   
			byte[] dataToDecrypt = File.ReadAllBytes(fileName);

			// Create an array to store the decrypted data in it   
			byte[] decryptedData;
			using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
			{
				// Set the private key of the algorithm   
				rsa.FromXmlString(privateKey);
				decryptedData = rsa.Decrypt(dataToDecrypt, false);
			}

			// Get the string value from the decryptedData byte array   
			UnicodeEncoding byteConverter = new UnicodeEncoding();
			return byteConverter.GetString(decryptedData);
		}
	}
}
