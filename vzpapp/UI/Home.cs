﻿using Microsoft.JScript;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using vzpapp.BAL;


namespace vzpapp.UI
{
    public partial class Home : Form
    {

        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);


        Reg uReg;
        public Home()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            SetAutoRotation(false);
            // this.Height = 500;
            // this.Owner.Close();
           

            if (!string.IsNullOrEmpty(BAL.Globals.user_sl))
                sllabel.Text = "Serial : EM" + BAL.Globals.user_sl;
            else
                sllabel.Text = "";

            uReg = new Reg();
            BAL.Globals.user_news = uReg.GetUserNewsCount();



            if (BAL.Globals.user_news > 0)
            {

                labelNews.Text = BAL.Globals.user_news.ToString();
            }
            else
                labelNews.Text = "";


           


            // textBox1.Text = BAL.Globals.currentUser.machine_id;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TimeSpan stime = TimeSpan.Parse(BAL.Globals.currentUser.service_stime);
            TimeSpan etime = TimeSpan.Parse(BAL.Globals.currentUser.service_etime);
            TimeSpan srvtime = TimeSpan.Parse(BAL.Globals.currentUser.server_time);


             if (string.IsNullOrEmpty(BAL.Globals.user_sl))
            {
                MessageBox.Show("Please Activate Services");

            }
            else if (DateTime.Compare(BAL.Globals.exp_date.Date, DateTime.Today) < 0)
            {

                MessageBox.Show("Your License is Expired!!");
            }
            else if (BAL.Globals.currentUser.service_status.Equals("0"))
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else if (etime <= srvtime)
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else if (stime >= srvtime)
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else
            {




                Services sv = new Services();
                //this.Hide();
                sv.Show();
                Thread.Sleep(1000);
                this.Close();



            }



        }

        private void label2_Click(object sender, EventArgs e)
        {

            button1_Click(sender, e);


        }

        private void label3_Click(object sender, EventArgs e)
        {
            News dn = new News();
            // this.Hide();
            dn.Show();
            Thread.Sleep(1000);
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            News dn = new News();
            BAL.Globals.news_stat = 0;
            //this.Hide();
            dn.Show();
            Thread.Sleep(1000);
            this.Close();
            //this.Dispose();
        }

        private void Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (new StackTrace().GetFrames().Any(x => x.GetMethod().Name == "Close"))
            {
                // MessageBox.Show("Closed by calling Close()");
            }
            else
                Application.Exit();


        }

        private void buttonChat_Click(object sender, EventArgs e)
        {
          

        }

        private void ticketButton_Click(object sender, EventArgs e)
        {
            TicketForm tk = new TicketForm();
            BAL.Globals.tickets_stat = 0;
            //this.Hide();
            tk.Show();
            Thread.Sleep(1000);
            this.Close();
        }
    }
}
