﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using vzpapp.BAL;

namespace vzpapp.UI
{
    public partial class TicketForm : Form
    {

        MsgBox newsmsg;
        MemoryStream responseStream;

        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);

        public TicketForm()
        {
            InitializeComponent();
        }

        private void TicketForm_Shown(object sender, EventArgs e)
        {
            try
            {
                if (Globals.tickets_stat == 0)
                {
                    newsmsg = new MsgBox("Getting Tickets for you", "tickets");
                    // newsmsg.showMsg();
                    newsmsg.ShowDialog();
                    Globals.tickets_stat = 1;

                }


                ticketDataGridView.DataSource = Globals.allTickets.Select(x => 
                new { x.id, x.createDate, x.subject, x.status,x.files,x.sfile,
                    x.cus_name,x.cus_license,x.cus_vin,x.cus_dtc,
                    x.v_producer,x.v_series,x.v_build,x.ecu_producer,x.ecu_build,x.ecu_file_type                    
                }
                
                
                ).ToList();
               // ticketDataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                //ticketDataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
               // ticketDataGridView.Columns[0].Visible = false;
               // ticketDataGridView.Columns[3].Visible = false;
                ticketDataGridView.EnableHeadersVisualStyles = false;
                ticketDataGridView.ClearSelection();


                ticketDataGridView.Columns.Add("DL", "Solution");


                ticketDataGridView.Columns[0].Width = 50;
                ticketDataGridView.Columns[1].Width = 100;
                ticketDataGridView.Columns[2].Width = 125;
                ticketDataGridView.Columns[3].Width = 60;
                DataGridViewColumn column_two = ticketDataGridView.Columns[4];
                ticketDataGridView.Columns[15].Width = 150;
                ticketDataGridView.Columns[16].Width = 100;
               
                column_two.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
             



                ticketDataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 13F, FontStyle.Bold);
                ticketDataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.RoyalBlue;
                ticketDataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

                // dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            


                ticketDataGridView.Columns[0].HeaderCell.Value = "Id";              
                ticketDataGridView.Columns[1].HeaderCell.Value = "Date";
                ticketDataGridView.Columns[2].HeaderCell.Value = "Subject";
                ticketDataGridView.Columns[3].HeaderCell.Value = "Status";
                ticketDataGridView.Columns[4].HeaderCell.Value = "File";
                ticketDataGridView.Columns[15].HeaderCell.Value = "File Type";
                //ticketDataGridView.Columns[5].HeaderCell.Value = "Solution";
                ticketDataGridView.Columns[5].Visible = false;
                ticketDataGridView.Columns[6].Visible = false;
                ticketDataGridView.Columns[7].Visible = false;
                ticketDataGridView.Columns[8].Visible = false;
                ticketDataGridView.Columns[9].Visible = false;
                ticketDataGridView.Columns[10].Visible = false;
                ticketDataGridView.Columns[11].Visible = false;
                ticketDataGridView.Columns[12].Visible = false;
                ticketDataGridView.Columns[13].Visible = false;
                ticketDataGridView.Columns[14].Visible = false;
             


                ticketDataGridView.AllowUserToAddRows = false;
                ticketDataGridView.AllowUserToDeleteRows = false;
                ticketDataGridView.AllowUserToOrderColumns = true;
                ticketDataGridView.ReadOnly = true;
                ticketDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                ticketDataGridView.MultiSelect = false;
                ticketDataGridView.AllowUserToResizeColumns = false;

                for (int i = 0; i < ticketDataGridView.Rows.Count; i++)
                {
                    String val = ticketDataGridView.Rows[i].Cells[3].Value.ToString();
                  


                    if (val == "Closed")
                    {
                      
                        ticketDataGridView.Rows[i].Cells[3].Style.ForeColor = Color.Green;
                        ticketDataGridView.Rows[i].Cells[3].Style.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Bold);
                        ticketDataGridView.Rows[i].Cells[16].Style.ForeColor = Color.Blue;
                        ticketDataGridView.Rows[i].Cells[16].Style.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Bold);

                        if (ticketDataGridView.Rows[i].Cells[5].Value != null)
                            ticketDataGridView.Rows[i].Cells[16].Value = "Download";


                    }
                    else
                    {
                      
                        ticketDataGridView.Rows[i].Cells[3].Style.ForeColor = Color.Red;
                        ticketDataGridView.Rows[i].Cells[3].Style.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Bold);

                    }
                }
            }
            catch (Exception nex)
            {

                MessageBox.Show("Tickets" + nex.Message);
            }

        }

        private void TicketForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (new StackTrace().GetFrames().Any(x => x.GetMethod().Name == "Close"))
            {
                // MessageBox.Show("Closed by calling Close()");
            }
            else
            {
                Home hme = new Home();
                //this.Hide();
                hme.Show();
                //this.Close();

            }
        }

        private void ticketDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = ticketDataGridView.CurrentCell.RowIndex;
            int colindex = e.ColumnIndex;
            String newsid = null;
            if (ticketDataGridView.Rows[rowindex].Cells[5].Value != null && colindex==16)
            {
                if (BAL.Globals.currentUser.GetUsbStatus() == 1)
                {
                    Globals.tickets_dl_stat = 0;
                    // newsmsg = new MsgBox("Getting Tickets for you", "dltickets");
                    // newsmsg.ShowDialog();
                    Cursor.Current = Cursors.WaitCursor;
                   

                    Tickets ticketFile = new Tickets();
                    newsid = ticketDataGridView.Rows[rowindex].Cells[5].Value.ToString();
                    responseStream = ticketFile.DownLoadTicket(newsid);
                    Globals.tickets_dl_stat = 1;
                    Cursor.Current = Cursors.Default;



                    string fileName = SetDownloadFileName();// Saved file name
                                                   //Let the user choose the file path to save
                    using (SaveFileDialog sfd = new SaveFileDialog())
                    {
                        sfd.FileName = fileName;
                        if (sfd.ShowDialog() != DialogResult.OK)
                        {
                            return;
                        }
                       
                        using (var fileStream = new FileStream(@"" + sfd.FileName, FileMode.Create, FileAccess.Write))
                        {
                            responseStream.Position = 0;
                            responseStream.CopyTo(fileStream);
                            MessageBox.Show(this, "File Suceessfully Saved");

                        }

                    }
                 }
                else
                {
                    MessageBox.Show(this, "USB Drive not found");
                }
            }
        }

        private void ticketDataGridView_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowindex =e.RowIndex;
            int colindex = e.ColumnIndex;
            if (rowindex>=0 && ticketDataGridView.Rows[rowindex].Cells[5].Value != null && colindex == 16)
            {
                ticketDataGridView.Cursor= Cursors.Hand;
            }
            else
            {

                ticketDataGridView.Cursor = Cursors.Default;
            }

        }

        private void ticketDataGridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = e.RowIndex; 
            int colindex = e.RowIndex;
            if (rowindex >= 0 && ticketDataGridView.Rows[rowindex].Cells[5].Value != null && colindex == 16)
            {
                ticketDataGridView.Cursor = Cursors.Default;
            }
           
        }

        public string SetDownloadFileName()
        {
            int rowindex = ticketDataGridView.CurrentCell.RowIndex;
            string fname = "EM_";

            string ecu_file_type = ticketDataGridView.Rows[rowindex].Cells[15].Value.ToString();
            string cus_name = ticketDataGridView.Rows[rowindex].Cells[6].Value.ToString();
            string cus_license = ticketDataGridView.Rows[rowindex].Cells[7].Value.ToString();
            string cus_vin = ticketDataGridView.Rows[rowindex].Cells[8].Value.ToString();
            string cus_dtc = ticketDataGridView.Rows[rowindex].Cells[9].Value.ToString();
            string v_producer = ticketDataGridView.Rows[rowindex].Cells[10].Value.ToString();
            string v_series = ticketDataGridView.Rows[rowindex].Cells[11].Value.ToString();
            string v_build = ticketDataGridView.Rows[rowindex].Cells[12].Value.ToString();
            string ecu_producer = ticketDataGridView.Rows[rowindex].Cells[13].Value.ToString();
            string ecu_build = ticketDataGridView.Rows[rowindex].Cells[15].Value.ToString();
            

            if (!String.IsNullOrEmpty(ecu_file_type))
                fname = fname + ecu_file_type.Replace(" ", "_");

            if (!String.IsNullOrEmpty(cus_name))
                fname = fname + "_" + cus_name.Replace(" ", "_");

            if (!String.IsNullOrEmpty(cus_license))
                fname = fname + "_" + cus_license.Replace(" ", "_");

            if (!String.IsNullOrEmpty(cus_vin))
                fname = fname + "_" + cus_vin.Replace(" ", "_");

            if (!String.IsNullOrEmpty(cus_dtc))
                fname = fname + "_" + cus_dtc.Replace(" ", "_");

            if (!String.IsNullOrEmpty(v_producer))
                fname = fname + "_" + v_producer.Replace(" ", "_");

            if (!String.IsNullOrEmpty(v_series))
                fname = fname + "_" + v_series.Replace(" ", "_");

            if (!String.IsNullOrEmpty(v_build))
                fname = fname + "_" + v_build.Replace(" ", "_");

            if (!String.IsNullOrEmpty(ecu_producer))
                fname = fname + "_" + ecu_producer.Replace(" ", "_");

            if (!String.IsNullOrEmpty(ecu_build))
                fname = fname + "_" + ecu_build.Replace(" ", "_");

            fname = Regex.Replace(fname, "[^a-zA-Z0-9_]+", "_", RegexOptions.Compiled);

            fname = fname + ".bin";


            return fname;
        }
    }
}
