﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using vzpapp.BAL;

namespace vzpapp.UI
{
    public partial class MsgBox : Form
    {
        Timer tmr1;
        DNews allNews;
        Tickets allTickets;
        Vehicles allVs;
        Ecu allEcus;
        BackgroundWorker load_worker;
        String msgType;
        public MsgBox(string txt, string mtype)
        {
            try
            {
                InitializeComponent();
                msgType = mtype;
                load_worker = new BackgroundWorker();
                load_worker.DoWork += new DoWorkEventHandler(load_worker_DoWork);
                load_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(load_worker_RunWorkerCompleted);
                load_worker.RunWorkerAsync();
                tmr1 = new Timer();
                tmr1.Start();
                label1.Text = txt;
                tmr1.Tick += tmr_Tick;

            }
            catch(Exception cex)
            {
                MessageBox.Show("MSG"+cex.Message);
                Globals.WriteToFile(Extensions.ExceptionInfo(cex));

            }



        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value == 100)
                progressBar1.Value = 1;

            progressBar1.Value++;

            if (this.msgType == "news" && Globals.allNews != null)
            {

                tmr1.Stop();
                this.Close();
            }
            if (this.msgType == "tickets" && Globals.allTickets != null)
            {

                tmr1.Stop();
                this.Close();
            }
            if (this.msgType == "dltickets" && Globals.tickets_dl_stat !=0)
            {

                tmr1.Stop();
                this.Close();
            }
            if (this.msgType == "ecu" && Globals.allECU != null && Globals.allVehilces != null)
            {

                tmr1.Stop();
                this.Close();
            }


            



        }

        private void load_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (this.msgType == "news")
            {
                LoadNews();
            }
            if (this.msgType == "tickets")
            {
                LoadTickets();
            }
            if (this.msgType == "ecu" && (Globals.allECU == null || Globals.allVehilces == null))
            {
                LoadEcuData();
            }
        }

        private void load_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Value = 100;
            if (this.msgType == "ecu" && (Globals.allECU == null || Globals.allVehilces == null))
            {
                tmr1.Stop();
                this.Close();
            }
            if (this.msgType == "tickets")
            {
                tmr1.Stop();
                this.Close();
            }

            if (this.msgType == "news")
            {
                tmr1.Stop();
                this.Close();
            }

        }




        private void DownloadTicket()
        {
          
                allTickets = new Tickets();
                Globals.allTickets =allTickets.GetUserTickets();

        }

        private void LoadTickets()
        {

            allTickets = new Tickets();
            Globals.allTickets = allTickets.GetUserTickets();

        }


        private void LoadNews()
        {

            allNews = new DNews();
            Globals.allNews = allNews.GetUserNews();



        }


        private void LoadEcuData ()
        {
            if (Globals.allVehilces == null)
            {
                allVs = new Vehicles();
                Globals.allVehilces = allVs.GetVehilces();

            }

            if (Globals.allECU == null)
            {
                allEcus = new Ecu();
                Globals.allECU = allEcus.GetEcu();

            }



        }

        private void MsgBox_FormClosing(object sender, FormClosingEventArgs e)
        {
           
            
        }
    }
}
