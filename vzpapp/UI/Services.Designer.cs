﻿namespace vzpapp.UI
{
    partial class Services
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Services));
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label3440 = new System.Windows.Forms.Label();
            this.label3539 = new System.Windows.Forms.Label();
            this.label3638 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label3935 = new System.Windows.Forms.Label();
            this.label4034 = new System.Windows.Forms.Label();
            this.label2133 = new System.Windows.Forms.Label();
            this.label2232 = new System.Windows.Forms.Label();
            this.label2331 = new System.Windows.Forms.Label();
            this.lock1 = new System.Windows.Forms.Label();
            this.lock2 = new System.Windows.Forms.Label();
            this.lock40 = new System.Windows.Forms.Label();
            this.lock39 = new System.Windows.Forms.Label();
            this.lock38 = new System.Windows.Forms.Label();
            this.lock37 = new System.Windows.Forms.Label();
            this.lock36 = new System.Windows.Forms.Label();
            this.lock35 = new System.Windows.Forms.Label();
            this.lock34 = new System.Windows.Forms.Label();
            this.lock33 = new System.Windows.Forms.Label();
            this.lock32 = new System.Windows.Forms.Label();
            this.lock31 = new System.Windows.Forms.Label();
            this.lock21 = new System.Windows.Forms.Label();
            this.lock20 = new System.Windows.Forms.Label();
            this.lock19 = new System.Windows.Forms.Label();
            this.lock18 = new System.Windows.Forms.Label();
            this.lock17 = new System.Windows.Forms.Label();
            this.lock16 = new System.Windows.Forms.Label();
            this.lock15 = new System.Windows.Forms.Label();
            this.lock14 = new System.Windows.Forms.Label();
            this.lock13 = new System.Windows.Forms.Label();
            this.lock12 = new System.Windows.Forms.Label();
            this.lock11 = new System.Windows.Forms.Label();
            this.lock10 = new System.Windows.Forms.Label();
            this.lock8 = new System.Windows.Forms.Label();
            this.lock9 = new System.Windows.Forms.Label();
            this.lock7 = new System.Windows.Forms.Label();
            this.lock6 = new System.Windows.Forms.Label();
            this.lock4 = new System.Windows.Forms.Label();
            this.lock5 = new System.Windows.Forms.Label();
            this.lock3 = new System.Windows.Forms.Label();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonNext = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label55 = new System.Windows.Forms.Label();
            this.lock55 = new System.Windows.Forms.Label();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.label54 = new System.Windows.Forms.Label();
            this.lock54 = new System.Windows.Forms.Label();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.lock24 = new System.Windows.Forms.Label();
            this.lock23 = new System.Windows.Forms.Label();
            this.lock22 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.lock28 = new System.Windows.Forms.Label();
            this.lock27 = new System.Windows.Forms.Label();
            this.lock26 = new System.Windows.Forms.Label();
            this.lock25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.buttonNext1 = new System.Windows.Forms.Button();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.lock30 = new System.Windows.Forms.Label();
            this.lock29 = new System.Windows.Forms.Label();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lock42 = new System.Windows.Forms.Label();
            this.lock41 = new System.Windows.Forms.Label();
            this.label3341 = new System.Windows.Forms.Label();
            this.label3242 = new System.Windows.Forms.Label();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.prefbutton3 = new System.Windows.Forms.Button();
            this.lock49 = new System.Windows.Forms.Label();
            this.lock48 = new System.Windows.Forms.Label();
            this.lock47 = new System.Windows.Forms.Label();
            this.lock46 = new System.Windows.Forms.Label();
            this.lock44 = new System.Windows.Forms.Label();
            this.lock45 = new System.Windows.Forms.Label();
            this.lock43 = new System.Windows.Forms.Label();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label3143 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label5044 = new System.Windows.Forms.Label();
            this.label4846 = new System.Windows.Forms.Label();
            this.label4945 = new System.Windows.Forms.Label();
            this.lock53 = new System.Windows.Forms.Label();
            this.lock52 = new System.Windows.Forms.Label();
            this.lock51 = new System.Windows.Forms.Label();
            this.lock50 = new System.Windows.Forms.Label();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(584, 491);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 29);
            this.label10.TabIndex = 59;
            this.label10.Text = "CheckSum-Fix";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(318, 491);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 29);
            this.label9.TabIndex = 58;
            this.label9.Text = "Catalyst Heating";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(49, 491);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 29);
            this.label8.TabIndex = 57;
            this.label8.Text = "Burbles Activation";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1620, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 42);
            this.label7.TabIndex = 56;
            this.label7.Text = "Boost Sensor";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1360, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 29);
            this.label6.TabIndex = 55;
            this.label6.Text = "Auto Start Stop";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1103, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 29);
            this.label5.TabIndex = 54;
            this.label5.Text = "Anti Theft Protection";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(837, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 29);
            this.label4.TabIndex = 53;
            this.label4.Text = "Antilag Fire";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(566, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 29);
            this.label3.TabIndex = 52;
            this.label3.Text = "A2L or Damos Make";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(313, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 29);
            this.label2.TabIndex = 51;
            this.label2.Text = "MB 164 Adblue Removal";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 29);
            this.label1.TabIndex = 50;
            this.label1.Text = "BMW 28A0 DTC OFF";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(306, 832);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 48);
            this.label16.TabIndex = 111;
            this.label16.Text = "DPF (exhaust particulate Filter)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1361, 832);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 29);
            this.label20.TabIndex = 110;
            this.label20.Text = "Evap Removal";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(1124, 832);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 29);
            this.label19.TabIndex = 109;
            this.label19.Text = "EGR-OFF";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label18.Location = new System.Drawing.Point(860, 832);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 18);
            this.label18.TabIndex = 108;
            this.label18.Text = "EGR";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(584, 832);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 29);
            this.label17.TabIndex = 107;
            this.label17.Text = "E85 Ready";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(67, 832);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 18);
            this.label15.TabIndex = 106;
            this.label15.Text = "DTC OFF";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1602, 491);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 29);
            this.label14.TabIndex = 105;
            this.label14.Text = "Decat (Cat OFF)";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1363, 491);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 49);
            this.label13.TabIndex = 104;
            this.label13.Text = "Cylinder On Demand Off";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1100, 491);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 29);
            this.label12.TabIndex = 103;
            this.label12.Text = "Cold Start Off";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(837, 491);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 49);
            this.label11.TabIndex = 102;
            this.label11.Text = "Cold Start Idle Iincrease Delete";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1620, 832);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(120, 48);
            this.label21.TabIndex = 112;
            this.label21.Text = "Exhaust Flap Control";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3440
            // 
            this.label3440.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3440.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3440.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3440.Location = new System.Drawing.Point(1611, 845);
            this.label3440.Name = "label3440";
            this.label3440.Size = new System.Drawing.Size(109, 40);
            this.label3440.TabIndex = 131;
            this.label3440.Text = "Readiness Calibration";
            this.label3440.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3539
            // 
            this.label3539.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3539.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3539.Location = new System.Drawing.Point(1362, 848);
            this.label3539.Name = "label3539";
            this.label3539.Size = new System.Drawing.Size(78, 29);
            this.label3539.TabIndex = 130;
            this.label3539.Text = "R button";
            this.label3539.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3638
            // 
            this.label3638.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3638.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3638.Location = new System.Drawing.Point(1075, 848);
            this.label3638.Name = "label3638";
            this.label3638.Size = new System.Drawing.Size(140, 40);
            this.label3638.TabIndex = 129;
            this.label3638.Text = "Power on Driving Mode (Sport Button)";
            this.label3638.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(836, 845);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 56);
            this.label37.TabIndex = 128;
            this.label37.Text = "Power on driving mode";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(39, 840);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(123, 45);
            this.label36.TabIndex = 127;
            this.label36.Text = "Pops and bangs Activation Stage 1";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3935
            // 
            this.label3935.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3935.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3935.Location = new System.Drawing.Point(1622, 476);
            this.label3935.Name = "label3935";
            this.label3935.Size = new System.Drawing.Size(84, 29);
            this.label3935.TabIndex = 126;
            this.label3935.Text = "No Lift Shift";
            this.label3935.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4034
            // 
            this.label4034.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4034.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4034.Location = new System.Drawing.Point(1340, 476);
            this.label4034.Name = "label4034";
            this.label4034.Size = new System.Drawing.Size(125, 29);
            this.label4034.TabIndex = 125;
            this.label4034.Text = "Maps Recognition";
            this.label4034.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2133
            // 
            this.label2133.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2133.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2133.Location = new System.Drawing.Point(1075, 476);
            this.label2133.Name = "label2133";
            this.label2133.Size = new System.Drawing.Size(137, 29);
            this.label2133.TabIndex = 124;
            this.label2133.Text = "Map Switching Code Add";
            this.label2133.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2232
            // 
            this.label2232.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2232.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2232.Location = new System.Drawing.Point(836, 476);
            this.label2232.Name = "label2232";
            this.label2232.Size = new System.Drawing.Size(128, 29);
            this.label2232.TabIndex = 123;
            this.label2232.Text = "Maf Removal";
            this.label2232.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2331
            // 
            this.label2331.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2331.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2331.Location = new System.Drawing.Point(562, 476);
            this.label2331.Name = "label2331";
            this.label2331.Size = new System.Drawing.Size(134, 46);
            this.label2331.TabIndex = 122;
            this.label2331.Text = "Left Foot Braking Activation";
            this.label2331.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lock1
            // 
            this.lock1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock1.Image = ((System.Drawing.Image)(resources.GetObject("lock1.Image")));
            this.lock1.Location = new System.Drawing.Point(133, 107);
            this.lock1.Name = "lock1";
            this.lock1.Size = new System.Drawing.Size(16, 19);
            this.lock1.TabIndex = 200;
            // 
            // lock2
            // 
            this.lock2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock2.Image = ((System.Drawing.Image)(resources.GetObject("lock2.Image")));
            this.lock2.Location = new System.Drawing.Point(390, 107);
            this.lock2.Name = "lock2";
            this.lock2.Size = new System.Drawing.Size(16, 19);
            this.lock2.TabIndex = 199;
            // 
            // lock40
            // 
            this.lock40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock40.Image = ((System.Drawing.Image)(resources.GetObject("lock40.Image")));
            this.lock40.Location = new System.Drawing.Point(1690, 808);
            this.lock40.Name = "lock40";
            this.lock40.Size = new System.Drawing.Size(16, 19);
            this.lock40.TabIndex = 185;
            // 
            // lock39
            // 
            this.lock39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock39.Image = ((System.Drawing.Image)(resources.GetObject("lock39.Image")));
            this.lock39.Location = new System.Drawing.Point(1431, 807);
            this.lock39.Name = "lock39";
            this.lock39.Size = new System.Drawing.Size(16, 19);
            this.lock39.TabIndex = 184;
            // 
            // lock38
            // 
            this.lock38.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock38.Image = ((System.Drawing.Image)(resources.GetObject("lock38.Image")));
            this.lock38.Location = new System.Drawing.Point(1168, 810);
            this.lock38.Name = "lock38";
            this.lock38.Size = new System.Drawing.Size(16, 19);
            this.lock38.TabIndex = 183;
            // 
            // lock37
            // 
            this.lock37.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock37.Image = ((System.Drawing.Image)(resources.GetObject("lock37.Image")));
            this.lock37.Location = new System.Drawing.Point(923, 810);
            this.lock37.Name = "lock37";
            this.lock37.Size = new System.Drawing.Size(16, 19);
            this.lock37.TabIndex = 182;
            // 
            // lock36
            // 
            this.lock36.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock36.Image = ((System.Drawing.Image)(resources.GetObject("lock36.Image")));
            this.lock36.Location = new System.Drawing.Point(133, 811);
            this.lock36.Name = "lock36";
            this.lock36.Size = new System.Drawing.Size(16, 19);
            this.lock36.TabIndex = 181;
            // 
            // lock35
            // 
            this.lock35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock35.Image = ((System.Drawing.Image)(resources.GetObject("lock35.Image")));
            this.lock35.Location = new System.Drawing.Point(1691, 441);
            this.lock35.Name = "lock35";
            this.lock35.Size = new System.Drawing.Size(16, 19);
            this.lock35.TabIndex = 180;
            // 
            // lock34
            // 
            this.lock34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock34.Image = ((System.Drawing.Image)(resources.GetObject("lock34.Image")));
            this.lock34.Location = new System.Drawing.Point(1431, 444);
            this.lock34.Name = "lock34";
            this.lock34.Size = new System.Drawing.Size(16, 19);
            this.lock34.TabIndex = 179;
            // 
            // lock33
            // 
            this.lock33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock33.Image = ((System.Drawing.Image)(resources.GetObject("lock33.Image")));
            this.lock33.Location = new System.Drawing.Point(1171, 441);
            this.lock33.Name = "lock33";
            this.lock33.Size = new System.Drawing.Size(16, 19);
            this.lock33.TabIndex = 178;
            // 
            // lock32
            // 
            this.lock32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock32.Image = ((System.Drawing.Image)(resources.GetObject("lock32.Image")));
            this.lock32.Location = new System.Drawing.Point(923, 442);
            this.lock32.Name = "lock32";
            this.lock32.Size = new System.Drawing.Size(16, 19);
            this.lock32.TabIndex = 177;
            // 
            // lock31
            // 
            this.lock31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock31.Image = ((System.Drawing.Image)(resources.GetObject("lock31.Image")));
            this.lock31.Location = new System.Drawing.Point(650, 444);
            this.lock31.Name = "lock31";
            this.lock31.Size = new System.Drawing.Size(16, 19);
            this.lock31.TabIndex = 176;
            // 
            // lock21
            // 
            this.lock21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock21.Image = ((System.Drawing.Image)(resources.GetObject("lock21.Image")));
            this.lock21.Location = new System.Drawing.Point(1707, 799);
            this.lock21.Name = "lock21";
            this.lock21.Size = new System.Drawing.Size(16, 19);
            this.lock21.TabIndex = 166;
            // 
            // lock20
            // 
            this.lock20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock20.Image = ((System.Drawing.Image)(resources.GetObject("lock20.Image")));
            this.lock20.Location = new System.Drawing.Point(1444, 801);
            this.lock20.Name = "lock20";
            this.lock20.Size = new System.Drawing.Size(16, 19);
            this.lock20.TabIndex = 165;
            // 
            // lock19
            // 
            this.lock19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock19.Image = ((System.Drawing.Image)(resources.GetObject("lock19.Image")));
            this.lock19.Location = new System.Drawing.Point(1187, 803);
            this.lock19.Name = "lock19";
            this.lock19.Size = new System.Drawing.Size(16, 19);
            this.lock19.TabIndex = 164;
            // 
            // lock18
            // 
            this.lock18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock18.Image = ((System.Drawing.Image)(resources.GetObject("lock18.Image")));
            this.lock18.Location = new System.Drawing.Point(926, 800);
            this.lock18.Name = "lock18";
            this.lock18.Size = new System.Drawing.Size(16, 19);
            this.lock18.TabIndex = 163;
            // 
            // lock17
            // 
            this.lock17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock17.Image = ((System.Drawing.Image)(resources.GetObject("lock17.Image")));
            this.lock17.Location = new System.Drawing.Point(658, 798);
            this.lock17.Name = "lock17";
            this.lock17.Size = new System.Drawing.Size(16, 19);
            this.lock17.TabIndex = 162;
            // 
            // lock16
            // 
            this.lock16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock16.Image = ((System.Drawing.Image)(resources.GetObject("lock16.Image")));
            this.lock16.Location = new System.Drawing.Point(396, 798);
            this.lock16.Name = "lock16";
            this.lock16.Size = new System.Drawing.Size(16, 19);
            this.lock16.TabIndex = 161;
            // 
            // lock15
            // 
            this.lock15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock15.Image = ((System.Drawing.Image)(resources.GetObject("lock15.Image")));
            this.lock15.Location = new System.Drawing.Point(134, 798);
            this.lock15.Name = "lock15";
            this.lock15.Size = new System.Drawing.Size(16, 19);
            this.lock15.TabIndex = 160;
            // 
            // lock14
            // 
            this.lock14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock14.Image = ((System.Drawing.Image)(resources.GetObject("lock14.Image")));
            this.lock14.Location = new System.Drawing.Point(1705, 455);
            this.lock14.Name = "lock14";
            this.lock14.Size = new System.Drawing.Size(16, 19);
            this.lock14.TabIndex = 159;
            // 
            // lock13
            // 
            this.lock13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock13.Image = ((System.Drawing.Image)(resources.GetObject("lock13.Image")));
            this.lock13.Location = new System.Drawing.Point(1444, 459);
            this.lock13.Name = "lock13";
            this.lock13.Size = new System.Drawing.Size(15, 16);
            this.lock13.TabIndex = 158;
            // 
            // lock12
            // 
            this.lock12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock12.Image = ((System.Drawing.Image)(resources.GetObject("lock12.Image")));
            this.lock12.Location = new System.Drawing.Point(1186, 455);
            this.lock12.Name = "lock12";
            this.lock12.Size = new System.Drawing.Size(16, 19);
            this.lock12.TabIndex = 157;
            // 
            // lock11
            // 
            this.lock11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock11.Image = ((System.Drawing.Image)(resources.GetObject("lock11.Image")));
            this.lock11.Location = new System.Drawing.Point(921, 459);
            this.lock11.Name = "lock11";
            this.lock11.Size = new System.Drawing.Size(16, 19);
            this.lock11.TabIndex = 156;
            // 
            // lock10
            // 
            this.lock10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock10.Image = ((System.Drawing.Image)(resources.GetObject("lock10.Image")));
            this.lock10.Location = new System.Drawing.Point(658, 458);
            this.lock10.Name = "lock10";
            this.lock10.Size = new System.Drawing.Size(16, 19);
            this.lock10.TabIndex = 155;
            // 
            // lock8
            // 
            this.lock8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock8.Image = ((System.Drawing.Image)(resources.GetObject("lock8.Image")));
            this.lock8.Location = new System.Drawing.Point(133, 458);
            this.lock8.Name = "lock8";
            this.lock8.Size = new System.Drawing.Size(16, 19);
            this.lock8.TabIndex = 154;
            // 
            // lock9
            // 
            this.lock9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock9.Image = ((System.Drawing.Image)(resources.GetObject("lock9.Image")));
            this.lock9.Location = new System.Drawing.Point(396, 458);
            this.lock9.Name = "lock9";
            this.lock9.Size = new System.Drawing.Size(16, 19);
            this.lock9.TabIndex = 153;
            // 
            // lock7
            // 
            this.lock7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock7.Image = ((System.Drawing.Image)(resources.GetObject("lock7.Image")));
            this.lock7.Location = new System.Drawing.Point(1701, 107);
            this.lock7.Name = "lock7";
            this.lock7.Size = new System.Drawing.Size(16, 19);
            this.lock7.TabIndex = 152;
            // 
            // lock6
            // 
            this.lock6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock6.Image = ((System.Drawing.Image)(resources.GetObject("lock6.Image")));
            this.lock6.Location = new System.Drawing.Point(1446, 107);
            this.lock6.Name = "lock6";
            this.lock6.Size = new System.Drawing.Size(16, 19);
            this.lock6.TabIndex = 151;
            // 
            // lock4
            // 
            this.lock4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock4.Image = ((System.Drawing.Image)(resources.GetObject("lock4.Image")));
            this.lock4.Location = new System.Drawing.Point(921, 107);
            this.lock4.Name = "lock4";
            this.lock4.Size = new System.Drawing.Size(16, 19);
            this.lock4.TabIndex = 150;
            // 
            // lock5
            // 
            this.lock5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock5.Image = ((System.Drawing.Image)(resources.GetObject("lock5.Image")));
            this.lock5.Location = new System.Drawing.Point(1186, 107);
            this.lock5.Name = "lock5";
            this.lock5.Size = new System.Drawing.Size(16, 19);
            this.lock5.TabIndex = 149;
            // 
            // lock3
            // 
            this.lock3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock3.Image = ((System.Drawing.Image)(resources.GetObject("lock3.Image")));
            this.lock3.Location = new System.Drawing.Point(656, 111);
            this.lock3.Name = "lock3";
            this.lock3.Size = new System.Drawing.Size(16, 19);
            this.lock3.TabIndex = 148;
            // 
            // pictureBox40
            // 
            this.pictureBox40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox40.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox40.Image")));
            this.pictureBox40.Location = new System.Drawing.Point(1625, 767);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(75, 55);
            this.pictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox40.TabIndex = 39;
            this.pictureBox40.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox33.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox33.Image")));
            this.pictureBox33.Location = new System.Drawing.Point(1105, 399);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(75, 55);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox33.TabIndex = 38;
            this.pictureBox33.TabStop = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox34.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox34.Image")));
            this.pictureBox34.Location = new System.Drawing.Point(1365, 399);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(75, 55);
            this.pictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox34.TabIndex = 37;
            this.pictureBox34.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox35.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox35.Image")));
            this.pictureBox35.Location = new System.Drawing.Point(1625, 399);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(75, 55);
            this.pictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox35.TabIndex = 36;
            this.pictureBox35.TabStop = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox36.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox36.Image")));
            this.pictureBox36.Location = new System.Drawing.Point(67, 767);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(75, 55);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox36.TabIndex = 35;
            this.pictureBox36.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox37.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox37.Image")));
            this.pictureBox37.Location = new System.Drawing.Point(857, 767);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(75, 55);
            this.pictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox37.TabIndex = 34;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox38
            // 
            this.pictureBox38.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox38.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox38.Image")));
            this.pictureBox38.Location = new System.Drawing.Point(1105, 767);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(75, 55);
            this.pictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox38.TabIndex = 33;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox39.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox39.Image")));
            this.pictureBox39.Location = new System.Drawing.Point(1365, 767);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(75, 55);
            this.pictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox39.TabIndex = 32;
            this.pictureBox39.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox32.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox32.Image")));
            this.pictureBox32.Location = new System.Drawing.Point(857, 398);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(75, 55);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox32.TabIndex = 31;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox31.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox31.Image")));
            this.pictureBox31.Location = new System.Drawing.Point(587, 398);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(75, 55);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox31.TabIndex = 30;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(1639, 757);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(75, 55);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox21.TabIndex = 20;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(1377, 757);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(75, 55);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox20.TabIndex = 19;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(1377, 416);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(75, 55);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 18;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(1639, 416);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(75, 55);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 17;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(67, 757);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(75, 55);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox15.TabIndex = 16;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(329, 757);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(75, 55);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox16.TabIndex = 15;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(591, 757);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(75, 55);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox17.TabIndex = 14;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(862, 757);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(75, 55);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox18.TabIndex = 13;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(1119, 757);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(75, 55);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox19.TabIndex = 12;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(1119, 416);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(75, 55);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 11;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(854, 416);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(75, 55);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 10;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(591, 416);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(75, 55);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 9;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(591, 65);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(75, 55);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(854, 65);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(75, 55);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(1119, 65);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(75, 55);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(1377, 65);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(75, 55);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(1639, 65);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(75, 55);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 4;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(67, 416);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(75, 55);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 3;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(329, 416);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(75, 55);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 2;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(329, 65);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(75, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(67, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1780, 1056);
            this.tabControl1.TabIndex = 201;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonNext);
            this.tabPage1.Controls.Add(this.lock1);
            this.tabPage1.Controls.Add(this.lock2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.lock21);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.lock20);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.lock19);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.lock18);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.lock17);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.lock16);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.lock15);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.lock14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.lock13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.lock12);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.lock11);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.lock10);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.lock8);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.lock9);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.lock7);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.lock6);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.lock4);
            this.tabPage1.Controls.Add(this.lock5);
            this.tabPage1.Controls.Add(this.lock3);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.pictureBox15);
            this.tabPage1.Controls.Add(this.pictureBox9);
            this.tabPage1.Controls.Add(this.pictureBox8);
            this.tabPage1.Controls.Add(this.pictureBox10);
            this.tabPage1.Controls.Add(this.pictureBox11);
            this.tabPage1.Controls.Add(this.pictureBox12);
            this.tabPage1.Controls.Add(this.pictureBox19);
            this.tabPage1.Controls.Add(this.pictureBox18);
            this.tabPage1.Controls.Add(this.pictureBox17);
            this.tabPage1.Controls.Add(this.pictureBox16);
            this.tabPage1.Controls.Add(this.pictureBox14);
            this.tabPage1.Controls.Add(this.pictureBox13);
            this.tabPage1.Controls.Add(this.pictureBox20);
            this.tabPage1.Controls.Add(this.pictureBox21);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1772, 1030);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Page-1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNext.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext.Location = new System.Drawing.Point(840, 953);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(116, 33);
            this.buttonNext.TabIndex = 201;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label55);
            this.tabPage2.Controls.Add(this.lock55);
            this.tabPage2.Controls.Add(this.pictureBox55);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Controls.Add(this.lock54);
            this.tabPage2.Controls.Add(this.pictureBox54);
            this.tabPage2.Controls.Add(this.lock24);
            this.tabPage2.Controls.Add(this.lock23);
            this.tabPage2.Controls.Add(this.lock22);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.pictureBox22);
            this.tabPage2.Controls.Add(this.pictureBox24);
            this.tabPage2.Controls.Add(this.pictureBox23);
            this.tabPage2.Controls.Add(this.lock28);
            this.tabPage2.Controls.Add(this.lock27);
            this.tabPage2.Controls.Add(this.lock26);
            this.tabPage2.Controls.Add(this.lock25);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.pictureBox28);
            this.tabPage2.Controls.Add(this.pictureBox27);
            this.tabPage2.Controls.Add(this.pictureBox26);
            this.tabPage2.Controls.Add(this.pictureBox25);
            this.tabPage2.Controls.Add(this.buttonNext1);
            this.tabPage2.Controls.Add(this.buttonPrev);
            this.tabPage2.Controls.Add(this.lock30);
            this.tabPage2.Controls.Add(this.lock29);
            this.tabPage2.Controls.Add(this.pictureBox30);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.pictureBox29);
            this.tabPage2.Controls.Add(this.lock40);
            this.tabPage2.Controls.Add(this.lock39);
            this.tabPage2.Controls.Add(this.lock38);
            this.tabPage2.Controls.Add(this.lock37);
            this.tabPage2.Controls.Add(this.lock36);
            this.tabPage2.Controls.Add(this.lock35);
            this.tabPage2.Controls.Add(this.lock34);
            this.tabPage2.Controls.Add(this.label2331);
            this.tabPage2.Controls.Add(this.lock33);
            this.tabPage2.Controls.Add(this.label2232);
            this.tabPage2.Controls.Add(this.lock32);
            this.tabPage2.Controls.Add(this.label2133);
            this.tabPage2.Controls.Add(this.lock31);
            this.tabPage2.Controls.Add(this.label4034);
            this.tabPage2.Controls.Add(this.label3935);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.label3638);
            this.tabPage2.Controls.Add(this.label3539);
            this.tabPage2.Controls.Add(this.label3440);
            this.tabPage2.Controls.Add(this.pictureBox34);
            this.tabPage2.Controls.Add(this.pictureBox31);
            this.tabPage2.Controls.Add(this.pictureBox32);
            this.tabPage2.Controls.Add(this.pictureBox39);
            this.tabPage2.Controls.Add(this.pictureBox38);
            this.tabPage2.Controls.Add(this.pictureBox37);
            this.tabPage2.Controls.Add(this.pictureBox36);
            this.tabPage2.Controls.Add(this.pictureBox35);
            this.tabPage2.Controls.Add(this.pictureBox33);
            this.tabPage2.Controls.Add(this.pictureBox40);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1772, 1030);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Page-2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label55
            // 
            this.label55.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(564, 845);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(123, 45);
            this.label55.TabIndex = 258;
            this.label55.Text = "Pops and bangs Activation Stage 3";
            this.label55.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lock55
            // 
            this.lock55.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock55.Image = ((System.Drawing.Image)(resources.GetObject("lock55.Image")));
            this.lock55.Location = new System.Drawing.Point(653, 811);
            this.lock55.Name = "lock55";
            this.lock55.Size = new System.Drawing.Size(16, 19);
            this.lock55.TabIndex = 257;
            // 
            // pictureBox55
            // 
            this.pictureBox55.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox55.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox55.Image")));
            this.pictureBox55.Location = new System.Drawing.Point(587, 767);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(75, 55);
            this.pictureBox55.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox55.TabIndex = 256;
            this.pictureBox55.TabStop = false;
            // 
            // label54
            // 
            this.label54.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(311, 845);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(123, 45);
            this.label54.TabIndex = 255;
            this.label54.Text = "Pops and bangs Activation Stage 2";
            this.label54.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lock54
            // 
            this.lock54.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock54.Image = ((System.Drawing.Image)(resources.GetObject("lock54.Image")));
            this.lock54.Location = new System.Drawing.Point(407, 811);
            this.lock54.Name = "lock54";
            this.lock54.Size = new System.Drawing.Size(16, 19);
            this.lock54.TabIndex = 254;
            // 
            // pictureBox54
            // 
            this.pictureBox54.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox54.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox54.Image")));
            this.pictureBox54.Location = new System.Drawing.Point(341, 767);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(75, 55);
            this.pictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox54.TabIndex = 253;
            this.pictureBox54.TabStop = false;
            // 
            // lock24
            // 
            this.lock24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock24.Image = ((System.Drawing.Image)(resources.GetObject("lock24.Image")));
            this.lock24.Location = new System.Drawing.Point(930, 111);
            this.lock24.Name = "lock24";
            this.lock24.Size = new System.Drawing.Size(16, 19);
            this.lock24.TabIndex = 252;
            // 
            // lock23
            // 
            this.lock23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock23.Image = ((System.Drawing.Image)(resources.GetObject("lock23.Image")));
            this.lock23.Location = new System.Drawing.Point(667, 111);
            this.lock23.Name = "lock23";
            this.lock23.Size = new System.Drawing.Size(16, 19);
            this.lock23.TabIndex = 251;
            // 
            // lock22
            // 
            this.lock22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock22.Image = ((System.Drawing.Image)(resources.GetObject("lock22.Image")));
            this.lock22.Location = new System.Drawing.Point(408, 107);
            this.lock22.Name = "lock22";
            this.lock22.Size = new System.Drawing.Size(16, 19);
            this.lock22.TabIndex = 250;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(328, 157);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 29);
            this.label22.TabIndex = 247;
            this.label22.Text = "Flap Removal";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(582, 157);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(114, 41);
            this.label23.TabIndex = 248;
            this.label23.Text = "Gearbox or TCU";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(850, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(96, 29);
            this.label24.TabIndex = 249;
            this.label24.Text = "GPF OR OPF Removal";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox22.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox22.Image")));
            this.pictureBox22.Location = new System.Drawing.Point(341, 65);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(75, 55);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox22.TabIndex = 244;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox24.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox24.Image")));
            this.pictureBox24.Location = new System.Drawing.Point(861, 65);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(75, 55);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox24.TabIndex = 245;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox23.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox23.Image")));
            this.pictureBox23.Location = new System.Drawing.Point(601, 65);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(75, 55);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox23.TabIndex = 246;
            this.pictureBox23.TabStop = false;
            // 
            // lock28
            // 
            this.lock28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock28.Image = ((System.Drawing.Image)(resources.GetObject("lock28.Image")));
            this.lock28.Location = new System.Drawing.Point(1690, 110);
            this.lock28.Name = "lock28";
            this.lock28.Size = new System.Drawing.Size(16, 19);
            this.lock28.TabIndex = 243;
            // 
            // lock27
            // 
            this.lock27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock27.Image = ((System.Drawing.Image)(resources.GetObject("lock27.Image")));
            this.lock27.Location = new System.Drawing.Point(1431, 111);
            this.lock27.Name = "lock27";
            this.lock27.Size = new System.Drawing.Size(16, 19);
            this.lock27.TabIndex = 242;
            // 
            // lock26
            // 
            this.lock26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock26.Image = ((System.Drawing.Image)(resources.GetObject("lock26.Image")));
            this.lock26.Location = new System.Drawing.Point(1179, 111);
            this.lock26.Name = "lock26";
            this.lock26.Size = new System.Drawing.Size(16, 19);
            this.lock26.TabIndex = 241;
            // 
            // lock25
            // 
            this.lock25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock25.Image = ((System.Drawing.Image)(resources.GetObject("lock25.Image")));
            this.lock25.Location = new System.Drawing.Point(130, 110);
            this.lock25.Name = "lock25";
            this.lock25.Size = new System.Drawing.Size(16, 19);
            this.lock25.TabIndex = 240;
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1326, 157);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(147, 29);
            this.label27.TabIndex = 236;
            this.label27.Text = "IMMO BYPASS";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1588, 157);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(161, 29);
            this.label28.TabIndex = 237;
            this.label28.Text = "Kick Down Deactivation";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(39, 157);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(123, 29);
            this.label25.TabIndex = 238;
            this.label25.Text = "Hard Rev Cut";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1103, 157);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 29);
            this.label26.TabIndex = 239;
            this.label26.Text = "Hot Start";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox28
            // 
            this.pictureBox28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox28.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox28.Image")));
            this.pictureBox28.Location = new System.Drawing.Point(1625, 65);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(75, 55);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox28.TabIndex = 232;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox27.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox27.Image")));
            this.pictureBox27.Location = new System.Drawing.Point(1363, 65);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(75, 55);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox27.TabIndex = 233;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox26.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox26.Image")));
            this.pictureBox26.Location = new System.Drawing.Point(1113, 65);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(75, 55);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox26.TabIndex = 234;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox25.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox25.Image")));
            this.pictureBox25.Location = new System.Drawing.Point(67, 65);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(75, 55);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox25.TabIndex = 235;
            this.pictureBox25.TabStop = false;
            // 
            // buttonNext1
            // 
            this.buttonNext1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonNext1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonNext1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNext1.Location = new System.Drawing.Point(921, 952);
            this.buttonNext1.Name = "buttonNext1";
            this.buttonNext1.Size = new System.Drawing.Size(116, 33);
            this.buttonNext1.TabIndex = 206;
            this.buttonNext1.Text = "Next";
            this.buttonNext1.UseVisualStyleBackColor = false;
            this.buttonNext1.Click += new System.EventHandler(this.buttonNext1_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPrev.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonPrev.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrev.Location = new System.Drawing.Point(772, 952);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(116, 33);
            this.buttonPrev.TabIndex = 205;
            this.buttonPrev.Text = "Previous";
            this.buttonPrev.UseVisualStyleBackColor = false;
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // lock30
            // 
            this.lock30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock30.Image = ((System.Drawing.Image)(resources.GetObject("lock30.Image")));
            this.lock30.Location = new System.Drawing.Point(407, 440);
            this.lock30.Name = "lock30";
            this.lock30.Size = new System.Drawing.Size(16, 19);
            this.lock30.TabIndex = 204;
            // 
            // lock29
            // 
            this.lock29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock29.Image = ((System.Drawing.Image)(resources.GetObject("lock29.Image")));
            this.lock29.Location = new System.Drawing.Point(130, 444);
            this.lock29.Name = "lock29";
            this.lock29.Size = new System.Drawing.Size(16, 19);
            this.lock29.TabIndex = 203;
            // 
            // pictureBox30
            // 
            this.pictureBox30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox30.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox30.Image")));
            this.pictureBox30.Location = new System.Drawing.Point(341, 398);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(75, 55);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox30.TabIndex = 200;
            this.pictureBox30.TabStop = false;
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(61, 476);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 45);
            this.label29.TabIndex = 201;
            this.label29.Text = "Lambda 02 Removal";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label30.Location = new System.Drawing.Point(328, 476);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(106, 46);
            this.label30.TabIndex = 202;
            this.label30.Text = "Launch Control AOD";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox29
            // 
            this.pictureBox29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox29.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox29.Image")));
            this.pictureBox29.Location = new System.Drawing.Point(67, 398);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(75, 55);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox29.TabIndex = 199;
            this.pictureBox29.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lock42);
            this.tabPage3.Controls.Add(this.lock41);
            this.tabPage3.Controls.Add(this.label3341);
            this.tabPage3.Controls.Add(this.label3242);
            this.tabPage3.Controls.Add(this.pictureBox42);
            this.tabPage3.Controls.Add(this.pictureBox41);
            this.tabPage3.Controls.Add(this.prefbutton3);
            this.tabPage3.Controls.Add(this.lock49);
            this.tabPage3.Controls.Add(this.lock48);
            this.tabPage3.Controls.Add(this.lock47);
            this.tabPage3.Controls.Add(this.lock46);
            this.tabPage3.Controls.Add(this.lock44);
            this.tabPage3.Controls.Add(this.lock45);
            this.tabPage3.Controls.Add(this.lock43);
            this.tabPage3.Controls.Add(this.pictureBox49);
            this.tabPage3.Controls.Add(this.pictureBox48);
            this.tabPage3.Controls.Add(this.pictureBox47);
            this.tabPage3.Controls.Add(this.pictureBox46);
            this.tabPage3.Controls.Add(this.pictureBox45);
            this.tabPage3.Controls.Add(this.pictureBox44);
            this.tabPage3.Controls.Add(this.pictureBox43);
            this.tabPage3.Controls.Add(this.label49);
            this.tabPage3.Controls.Add(this.label48);
            this.tabPage3.Controls.Add(this.label3143);
            this.tabPage3.Controls.Add(this.label47);
            this.tabPage3.Controls.Add(this.label5044);
            this.tabPage3.Controls.Add(this.label4846);
            this.tabPage3.Controls.Add(this.label4945);
            this.tabPage3.Controls.Add(this.lock53);
            this.tabPage3.Controls.Add(this.lock52);
            this.tabPage3.Controls.Add(this.lock51);
            this.tabPage3.Controls.Add(this.lock50);
            this.tabPage3.Controls.Add(this.pictureBox50);
            this.tabPage3.Controls.Add(this.pictureBox53);
            this.tabPage3.Controls.Add(this.pictureBox52);
            this.tabPage3.Controls.Add(this.pictureBox51);
            this.tabPage3.Controls.Add(this.label53);
            this.tabPage3.Controls.Add(this.label52);
            this.tabPage3.Controls.Add(this.label51);
            this.tabPage3.Controls.Add(this.label50);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1772, 1030);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Page-3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lock42
            // 
            this.lock42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock42.Image = ((System.Drawing.Image)(resources.GetObject("lock42.Image")));
            this.lock42.Location = new System.Drawing.Point(418, 141);
            this.lock42.Name = "lock42";
            this.lock42.Size = new System.Drawing.Size(16, 19);
            this.lock42.TabIndex = 259;
            // 
            // lock41
            // 
            this.lock41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock41.Image = ((System.Drawing.Image)(resources.GetObject("lock41.Image")));
            this.lock41.Location = new System.Drawing.Point(153, 143);
            this.lock41.Name = "lock41";
            this.lock41.Size = new System.Drawing.Size(16, 19);
            this.lock41.TabIndex = 258;
            // 
            // label3341
            // 
            this.label3341.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3341.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3341.Location = new System.Drawing.Point(39, 181);
            this.label3341.Name = "label3341";
            this.label3341.Size = new System.Drawing.Size(173, 57);
            this.label3341.TabIndex = 256;
            this.label3341.Text = "SCR Catalyst (deNox catalyst) delete and AdBlue_or_Urea_delete";
            this.label3341.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3242
            // 
            this.label3242.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3242.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3242.Location = new System.Drawing.Point(336, 181);
            this.label3242.Name = "label3242";
            this.label3242.Size = new System.Drawing.Size(103, 41);
            this.label3242.TabIndex = 257;
            this.label3242.Text = "Spa Removal";
            this.label3242.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox42
            // 
            this.pictureBox42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox42.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox42.Image")));
            this.pictureBox42.Location = new System.Drawing.Point(348, 100);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(75, 55);
            this.pictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox42.TabIndex = 255;
            this.pictureBox42.TabStop = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox41.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox41.Image")));
            this.pictureBox41.Location = new System.Drawing.Point(88, 100);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(75, 55);
            this.pictureBox41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox41.TabIndex = 254;
            this.pictureBox41.TabStop = false;
            // 
            // prefbutton3
            // 
            this.prefbutton3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.prefbutton3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.prefbutton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prefbutton3.Location = new System.Drawing.Point(849, 948);
            this.prefbutton3.Name = "prefbutton3";
            this.prefbutton3.Size = new System.Drawing.Size(116, 33);
            this.prefbutton3.TabIndex = 253;
            this.prefbutton3.Text = "Previous";
            this.prefbutton3.UseVisualStyleBackColor = false;
            this.prefbutton3.Click += new System.EventHandler(this.prefbutton3_Click);
            // 
            // lock49
            // 
            this.lock49.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock49.Image = ((System.Drawing.Image)(resources.GetObject("lock49.Image")));
            this.lock49.Location = new System.Drawing.Point(390, 497);
            this.lock49.Name = "lock49";
            this.lock49.Size = new System.Drawing.Size(16, 19);
            this.lock49.TabIndex = 252;
            // 
            // lock48
            // 
            this.lock48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock48.Image = ((System.Drawing.Image)(resources.GetObject("lock48.Image")));
            this.lock48.Location = new System.Drawing.Point(147, 496);
            this.lock48.Name = "lock48";
            this.lock48.Size = new System.Drawing.Size(16, 19);
            this.lock48.TabIndex = 251;
            // 
            // lock47
            // 
            this.lock47.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock47.Image = ((System.Drawing.Image)(resources.GetObject("lock47.Image")));
            this.lock47.Location = new System.Drawing.Point(1717, 145);
            this.lock47.Name = "lock47";
            this.lock47.Size = new System.Drawing.Size(16, 19);
            this.lock47.TabIndex = 250;
            // 
            // lock46
            // 
            this.lock46.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock46.Image = ((System.Drawing.Image)(resources.GetObject("lock46.Image")));
            this.lock46.Location = new System.Drawing.Point(1459, 145);
            this.lock46.Name = "lock46";
            this.lock46.Size = new System.Drawing.Size(16, 19);
            this.lock46.TabIndex = 249;
            // 
            // lock44
            // 
            this.lock44.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock44.Image = ((System.Drawing.Image)(resources.GetObject("lock44.Image")));
            this.lock44.Location = new System.Drawing.Point(938, 140);
            this.lock44.Name = "lock44";
            this.lock44.Size = new System.Drawing.Size(16, 19);
            this.lock44.TabIndex = 248;
            // 
            // lock45
            // 
            this.lock45.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock45.Image = ((System.Drawing.Image)(resources.GetObject("lock45.Image")));
            this.lock45.Location = new System.Drawing.Point(1201, 142);
            this.lock45.Name = "lock45";
            this.lock45.Size = new System.Drawing.Size(16, 19);
            this.lock45.TabIndex = 247;
            // 
            // lock43
            // 
            this.lock43.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock43.Image = ((System.Drawing.Image)(resources.GetObject("lock43.Image")));
            this.lock43.Location = new System.Drawing.Point(663, 141);
            this.lock43.Name = "lock43";
            this.lock43.Size = new System.Drawing.Size(16, 19);
            this.lock43.TabIndex = 246;
            // 
            // pictureBox49
            // 
            this.pictureBox49.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox49.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox49.Image")));
            this.pictureBox49.Location = new System.Drawing.Point(324, 452);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(75, 55);
            this.pictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox49.TabIndex = 232;
            this.pictureBox49.TabStop = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox48.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox48.Image")));
            this.pictureBox48.Location = new System.Drawing.Point(82, 452);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(75, 55);
            this.pictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox48.TabIndex = 233;
            this.pictureBox48.TabStop = false;
            // 
            // pictureBox47
            // 
            this.pictureBox47.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox47.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox47.Image")));
            this.pictureBox47.Location = new System.Drawing.Point(1651, 100);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(75, 55);
            this.pictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox47.TabIndex = 234;
            this.pictureBox47.TabStop = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox46.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox46.Image")));
            this.pictureBox46.Location = new System.Drawing.Point(1394, 100);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(75, 55);
            this.pictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox46.TabIndex = 235;
            this.pictureBox46.TabStop = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox45.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox45.Image")));
            this.pictureBox45.Location = new System.Drawing.Point(1134, 100);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(75, 55);
            this.pictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox45.TabIndex = 236;
            this.pictureBox45.TabStop = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox44.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox44.Image")));
            this.pictureBox44.Location = new System.Drawing.Point(874, 100);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(75, 55);
            this.pictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox44.TabIndex = 237;
            this.pictureBox44.TabStop = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox43.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox43.Image")));
            this.pictureBox43.Location = new System.Drawing.Point(599, 100);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(75, 55);
            this.pictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox43.TabIndex = 238;
            this.pictureBox43.TabStop = false;
            // 
            // label49
            // 
            this.label49.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(306, 525);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(122, 29);
            this.label49.TabIndex = 245;
            this.label49.Text = "Swirl Flap OFF";
            this.label49.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label48
            // 
            this.label48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(61, 525);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(122, 29);
            this.label48.TabIndex = 244;
            this.label48.Text = "Start Stop Add";
            this.label48.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3143
            // 
            this.label3143.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3143.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3143.Location = new System.Drawing.Point(580, 183);
            this.label3143.Name = "label3143";
            this.label3143.Size = new System.Drawing.Size(116, 29);
            this.label3143.TabIndex = 239;
            this.label3143.Text = "Sport Display";
            this.label3143.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label47
            // 
            this.label47.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(1634, 178);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(110, 44);
            this.label47.TabIndex = 243;
            this.label47.Text = "Start and Stop OFF";
            this.label47.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5044
            // 
            this.label5044.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5044.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5044.Location = new System.Drawing.Point(851, 181);
            this.label5044.Name = "label5044";
            this.label5044.Size = new System.Drawing.Size(114, 49);
            this.label5044.TabIndex = 240;
            this.label5044.Text = "Stage1 Stock Car";
            this.label5044.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4846
            // 
            this.label4846.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4846.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4846.Location = new System.Drawing.Point(1364, 174);
            this.label4846.Name = "label4846";
            this.label4846.Size = new System.Drawing.Size(125, 49);
            this.label4846.TabIndex = 242;
            this.label4846.Text = "Stage 3 Turbo Upgrade";
            this.label4846.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4945
            // 
            this.label4945.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4945.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4945.Location = new System.Drawing.Point(1091, 183);
            this.label4945.Name = "label4945";
            this.label4945.Size = new System.Drawing.Size(148, 38);
            this.label4945.TabIndex = 241;
            this.label4945.Text = "Stage2 Modified Intake and Exhaust system";
            this.label4945.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lock53
            // 
            this.lock53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock53.Image = ((System.Drawing.Image)(resources.GetObject("lock53.Image")));
            this.lock53.Location = new System.Drawing.Point(1441, 495);
            this.lock53.Name = "lock53";
            this.lock53.Size = new System.Drawing.Size(16, 19);
            this.lock53.TabIndex = 210;
            // 
            // lock52
            // 
            this.lock52.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock52.Image = ((System.Drawing.Image)(resources.GetObject("lock52.Image")));
            this.lock52.Location = new System.Drawing.Point(1185, 494);
            this.lock52.Name = "lock52";
            this.lock52.Size = new System.Drawing.Size(16, 19);
            this.lock52.TabIndex = 209;
            // 
            // lock51
            // 
            this.lock51.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock51.Image = ((System.Drawing.Image)(resources.GetObject("lock51.Image")));
            this.lock51.Location = new System.Drawing.Point(928, 495);
            this.lock51.Name = "lock51";
            this.lock51.Size = new System.Drawing.Size(16, 19);
            this.lock51.TabIndex = 208;
            // 
            // lock50
            // 
            this.lock50.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lock50.Image = ((System.Drawing.Image)(resources.GetObject("lock50.Image")));
            this.lock50.Location = new System.Drawing.Point(663, 494);
            this.lock50.Name = "lock50";
            this.lock50.Size = new System.Drawing.Size(16, 19);
            this.lock50.TabIndex = 207;
            // 
            // pictureBox50
            // 
            this.pictureBox50.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox50.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox50.Image")));
            this.pictureBox50.Location = new System.Drawing.Point(599, 452);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(75, 55);
            this.pictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox50.TabIndex = 199;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox53.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox53.Image")));
            this.pictureBox53.Location = new System.Drawing.Point(1376, 452);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(75, 55);
            this.pictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox53.TabIndex = 206;
            this.pictureBox53.TabStop = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox52.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox52.Image")));
            this.pictureBox52.Location = new System.Drawing.Point(1119, 452);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(75, 55);
            this.pictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox52.TabIndex = 205;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox51.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox51.Image")));
            this.pictureBox51.Location = new System.Drawing.Point(859, 452);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(75, 55);
            this.pictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox51.TabIndex = 204;
            this.pictureBox51.TabStop = false;
            // 
            // label53
            // 
            this.label53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(1352, 525);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(120, 29);
            this.label53.TabIndex = 203;
            this.label53.Text = "VMAX Off";
            this.label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label52
            // 
            this.label52.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(1096, 525);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(127, 35);
            this.label52.TabIndex = 202;
            this.label52.Text = "VAS Import";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label51
            // 
            this.label51.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(829, 525);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(136, 35);
            this.label51.TabIndex = 201;
            this.label51.Text = "TVA Removal";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label50
            // 
            this.label50.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label50.Location = new System.Drawing.Point(573, 525);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(124, 40);
            this.label50.TabIndex = 200;
            this.label50.Text = "Torque Monitoring Disable";
            this.label50.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Services
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1784, 1061);
            this.Controls.Add(this.tabControl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Services";
            this.Text = "Services";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Services_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label3440;
        private System.Windows.Forms.Label label3539;
        private System.Windows.Forms.Label label3638;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label3935;
        private System.Windows.Forms.Label label4034;
        private System.Windows.Forms.Label label2133;
        private System.Windows.Forms.Label label2232;
        private System.Windows.Forms.Label label2331;
        private System.Windows.Forms.Label lock3;
        private System.Windows.Forms.Label lock5;
        private System.Windows.Forms.Label lock4;
        private System.Windows.Forms.Label lock6;
        private System.Windows.Forms.Label lock7;
        private System.Windows.Forms.Label lock9;
        private System.Windows.Forms.Label lock8;
        private System.Windows.Forms.Label lock10;
        private System.Windows.Forms.Label lock11;
        private System.Windows.Forms.Label lock12;
        private System.Windows.Forms.Label lock13;
        private System.Windows.Forms.Label lock14;
        private System.Windows.Forms.Label lock15;
        private System.Windows.Forms.Label lock16;
        private System.Windows.Forms.Label lock17;
        private System.Windows.Forms.Label lock18;
        private System.Windows.Forms.Label lock19;
        private System.Windows.Forms.Label lock20;
        private System.Windows.Forms.Label lock21;
        private System.Windows.Forms.Label lock31;
        private System.Windows.Forms.Label lock32;
        private System.Windows.Forms.Label lock33;
        private System.Windows.Forms.Label lock34;
        private System.Windows.Forms.Label lock35;
        private System.Windows.Forms.Label lock36;
        private System.Windows.Forms.Label lock37;
        private System.Windows.Forms.Label lock38;
        private System.Windows.Forms.Label lock39;
        private System.Windows.Forms.Label lock40;
        private System.Windows.Forms.Label lock2;
        private System.Windows.Forms.Label lock1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lock30;
        private System.Windows.Forms.Label lock29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lock53;
        private System.Windows.Forms.Label lock52;
        private System.Windows.Forms.Label lock51;
        private System.Windows.Forms.Label lock50;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button buttonNext1;
        private System.Windows.Forms.Label lock24;
        private System.Windows.Forms.Label lock23;
        private System.Windows.Forms.Label lock22;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Label lock28;
        private System.Windows.Forms.Label lock27;
        private System.Windows.Forms.Label lock26;
        private System.Windows.Forms.Label lock25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.Label lock49;
        private System.Windows.Forms.Label lock48;
        private System.Windows.Forms.Label lock47;
        private System.Windows.Forms.Label lock46;
        private System.Windows.Forms.Label lock44;
        private System.Windows.Forms.Label lock45;
        private System.Windows.Forms.Label lock43;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label3143;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label5044;
        private System.Windows.Forms.Label label4846;
        private System.Windows.Forms.Label label4945;
        private System.Windows.Forms.Button prefbutton3;
        private System.Windows.Forms.Label lock42;
        private System.Windows.Forms.Label lock41;
        private System.Windows.Forms.Label label3341;
        private System.Windows.Forms.Label label3242;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.Label lock54;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label lock55;
        private System.Windows.Forms.PictureBox pictureBox55;
    }
}