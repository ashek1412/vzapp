﻿namespace vzpapp.UI
{
    partial class EcuEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EcuEntryForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.yearComboBox = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.modelComboBox = new System.Windows.Forms.ComboBox();
            this.buildComboBox = new System.Windows.Forms.ComboBox();
            this.seriesComboBox = new System.Windows.Forms.ComboBox();
            this.producerComboBox = new System.Windows.Forms.ComboBox();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.versiontextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.softwaretextBox = new System.Windows.Forms.TextBox();
            this.ecutextBox = new System.Windows.Forms.TextBox();
            this.pecutextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buildgrpComboBox = new System.Windows.Forms.ComboBox();
            this.ecuProducercComboBox = new System.Windows.Forms.ComboBox();
            this.useComboBox = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CusnametextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtctextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.vinTextBox = new System.Windows.Forms.TextBox();
            this.licenseTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.labelStat = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button3 = new System.Windows.Forms.Button();
            this.processButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.uploadTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.labelHeader = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.yearComboBox);
            this.groupBox1.Controls.Add(this.comboBox6);
            this.groupBox1.Controls.Add(this.modelComboBox);
            this.groupBox1.Controls.Add(this.buildComboBox);
            this.groupBox1.Controls.Add(this.seriesComboBox);
            this.groupBox1.Controls.Add(this.producerComboBox);
            this.groupBox1.Controls.Add(this.typeComboBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(251, 340);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(598, 384);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vehicle";
            // 
            // yearComboBox
            // 
            this.yearComboBox.FormattingEnabled = true;
            this.yearComboBox.Location = new System.Drawing.Point(164, 320);
            this.yearComboBox.Name = "yearComboBox";
            this.yearComboBox.Size = new System.Drawing.Size(291, 32);
            this.yearComboBox.TabIndex = 7;
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(164, 271);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(291, 32);
            this.comboBox6.TabIndex = 10;
            // 
            // modelComboBox
            // 
            this.modelComboBox.FormattingEnabled = true;
            this.modelComboBox.Location = new System.Drawing.Point(164, 222);
            this.modelComboBox.Name = "modelComboBox";
            this.modelComboBox.Size = new System.Drawing.Size(291, 32);
            this.modelComboBox.TabIndex = 9;
            // 
            // buildComboBox
            // 
            this.buildComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.buildComboBox.FormattingEnabled = true;
            this.buildComboBox.Location = new System.Drawing.Point(164, 173);
            this.buildComboBox.Name = "buildComboBox";
            this.buildComboBox.Size = new System.Drawing.Size(291, 32);
            this.buildComboBox.TabIndex = 8;
            this.buildComboBox.SelectedIndexChanged += new System.EventHandler(this.buildComboBox_SelectedIndexChanged);
            // 
            // seriesComboBox
            // 
            this.seriesComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.seriesComboBox.FormattingEnabled = true;
            this.seriesComboBox.Location = new System.Drawing.Point(164, 124);
            this.seriesComboBox.Name = "seriesComboBox";
            this.seriesComboBox.Size = new System.Drawing.Size(291, 32);
            this.seriesComboBox.TabIndex = 7;
            this.seriesComboBox.SelectedIndexChanged += new System.EventHandler(this.seriesComboBox_SelectedIndexChanged);
            // 
            // producerComboBox
            // 
            this.producerComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.producerComboBox.FormattingEnabled = true;
            this.producerComboBox.Location = new System.Drawing.Point(164, 75);
            this.producerComboBox.Name = "producerComboBox";
            this.producerComboBox.Size = new System.Drawing.Size(291, 32);
            this.producerComboBox.TabIndex = 6;
            this.producerComboBox.SelectedIndexChanged += new System.EventHandler(this.producerComboBox_SelectedIndexChanged);
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(164, 26);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(291, 32);
            this.typeComboBox.TabIndex = 5;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(23, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 22);
            this.label7.TabIndex = 6;
            this.label7.Text = "Model Year :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 275);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 22);
            this.label6.TabIndex = 5;
            this.label6.Text = "Characteristics :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(65, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 22);
            this.label5.TabIndex = 4;
            this.label5.Text = "Model :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(65, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "Build :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(65, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Series :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(35, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Producer :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(47, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox3.Controls.Add(this.versiontextBox);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.softwaretextBox);
            this.groupBox3.Controls.Add(this.ecutextBox);
            this.groupBox3.Controls.Add(this.pecutextBox);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.buildgrpComboBox);
            this.groupBox3.Controls.Add(this.ecuProducercComboBox);
            this.groupBox3.Controls.Add(this.useComboBox);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(941, 340);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(623, 384);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ECU";
            // 
            // versiontextBox
            // 
            this.versiontextBox.Location = new System.Drawing.Point(229, 324);
            this.versiontextBox.Name = "versiontextBox";
            this.versiontextBox.Size = new System.Drawing.Size(344, 29);
            this.versiontextBox.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(86, 330);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 22);
            this.label9.TabIndex = 18;
            this.label9.Text = "Version :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // softwaretextBox
            // 
            this.softwaretextBox.Location = new System.Drawing.Point(229, 278);
            this.softwaretextBox.Name = "softwaretextBox";
            this.softwaretextBox.Size = new System.Drawing.Size(344, 29);
            this.softwaretextBox.TabIndex = 16;
            // 
            // ecutextBox
            // 
            this.ecutextBox.Location = new System.Drawing.Point(229, 230);
            this.ecutextBox.Name = "ecutextBox";
            this.ecutextBox.Size = new System.Drawing.Size(344, 29);
            this.ecutextBox.TabIndex = 15;
            // 
            // pecutextBox
            // 
            this.pecutextBox.Location = new System.Drawing.Point(229, 181);
            this.pecutextBox.Name = "pecutextBox";
            this.pecutextBox.Size = new System.Drawing.Size(344, 29);
            this.pecutextBox.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(68, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 22);
            this.label8.TabIndex = 14;
            this.label8.Text = "ECU -Nr. ECU :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buildgrpComboBox
            // 
            this.buildgrpComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.buildgrpComboBox.FormattingEnabled = true;
            this.buildgrpComboBox.Location = new System.Drawing.Point(229, 134);
            this.buildgrpComboBox.Name = "buildgrpComboBox";
            this.buildgrpComboBox.Size = new System.Drawing.Size(344, 32);
            this.buildgrpComboBox.TabIndex = 13;
            // 
            // ecuProducercComboBox
            // 
            this.ecuProducercComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ecuProducercComboBox.FormattingEnabled = true;
            this.ecuProducercComboBox.Location = new System.Drawing.Point(229, 82);
            this.ecuProducercComboBox.Name = "ecuProducercComboBox";
            this.ecuProducercComboBox.Size = new System.Drawing.Size(344, 32);
            this.ecuProducercComboBox.TabIndex = 12;
            this.ecuProducercComboBox.SelectedIndexChanged += new System.EventHandler(this.ecuProducercComboBox_SelectedIndexChanged);
            // 
            // useComboBox
            // 
            this.useComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.useComboBox.FormattingEnabled = true;
            this.useComboBox.Location = new System.Drawing.Point(229, 30);
            this.useComboBox.Name = "useComboBox";
            this.useComboBox.Size = new System.Drawing.Size(344, 32);
            this.useComboBox.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(86, 282);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 22);
            this.label15.TabIndex = 6;
            this.label15.Text = "Software :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(74, 185);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(139, 22);
            this.label17.TabIndex = 4;
            this.label17.Text = "ECU -Nr. Prod :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(86, 139);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 22);
            this.label19.TabIndex = 2;
            this.label19.Text = "Build :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(86, 87);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(127, 22);
            this.label20.TabIndex = 1;
            this.label20.Text = "Producer :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(86, 36);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(127, 22);
            this.label21.TabIndex = 0;
            this.label21.Text = "Use :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox2.Controls.Add(this.CusnametextBox);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtctextBox);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.vinTextBox);
            this.groupBox2.Controls.Add(this.licenseTextBox);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(473, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(821, 233);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer";
            // 
            // CusnametextBox
            // 
            this.CusnametextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CusnametextBox.Location = new System.Drawing.Point(244, 32);
            this.CusnametextBox.Name = "CusnametextBox";
            this.CusnametextBox.Size = new System.Drawing.Size(359, 29);
            this.CusnametextBox.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(119, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 22);
            this.label14.TabIndex = 24;
            this.label14.Text = "Name :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtctextBox
            // 
            this.dtctextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtctextBox.Location = new System.Drawing.Point(244, 176);
            this.dtctextBox.Name = "dtctextBox";
            this.dtctextBox.Size = new System.Drawing.Size(359, 29);
            this.dtctextBox.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(97, 180);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 22);
            this.label13.TabIndex = 22;
            this.label13.Text = "DTC Code :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // vinTextBox
            // 
            this.vinTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vinTextBox.Location = new System.Drawing.Point(244, 127);
            this.vinTextBox.Name = "vinTextBox";
            this.vinTextBox.Size = new System.Drawing.Size(359, 29);
            this.vinTextBox.TabIndex = 3;
            // 
            // licenseTextBox
            // 
            this.licenseTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.licenseTextBox.Location = new System.Drawing.Point(244, 79);
            this.licenseTextBox.Name = "licenseTextBox";
            this.licenseTextBox.Size = new System.Drawing.Size(359, 29);
            this.licenseTextBox.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(138, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 22);
            this.label11.TabIndex = 14;
            this.label11.Text = "VIN :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(73, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 22);
            this.label10.TabIndex = 14;
            this.label10.Text = "License Plate :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox4.AutoSize = true;
            this.groupBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox4.Controls.Add(this.cancelButton);
            this.groupBox4.Controls.Add(this.labelStat);
            this.groupBox4.Controls.Add(this.saveButton);
            this.groupBox4.Controls.Add(this.progressBar1);
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.processButton);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.uploadTextBox);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(251, 747);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1313, 270);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "File";
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.Red;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(702, 189);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(133, 56);
            this.cancelButton.TabIndex = 27;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // labelStat
            // 
            this.labelStat.BackColor = System.Drawing.Color.Transparent;
            this.labelStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStat.ForeColor = System.Drawing.Color.DeepPink;
            this.labelStat.Location = new System.Drawing.Point(27, 29);
            this.labelStat.Name = "labelStat";
            this.labelStat.Size = new System.Drawing.Size(1270, 33);
            this.labelStat.TabIndex = 26;
            this.labelStat.Text = "Select ECU file to Process";
            this.labelStat.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.LightGreen;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Black;
            this.saveButton.Location = new System.Drawing.Point(513, 189);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(146, 56);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save to USB";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Visible = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(167, 79);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(958, 36);
            this.progressBar1.TabIndex = 19;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(869, 189);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 56);
            this.button3.TabIndex = 19;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // processButton
            // 
            this.processButton.AllowDrop = true;
            this.processButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.processButton.Enabled = false;
            this.processButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.processButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.processButton.ForeColor = System.Drawing.Color.Green;
            this.processButton.Location = new System.Drawing.Point(338, 189);
            this.processButton.Name = "processButton";
            this.processButton.Size = new System.Drawing.Size(149, 56);
            this.processButton.TabIndex = 18;
            this.processButton.Text = "Process";
            this.processButton.UseVisualStyleBackColor = false;
            this.processButton.Click += new System.EventHandler(this.processButton_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1018, 133);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 47);
            this.button1.TabIndex = 17;
            this.button1.TabStop = false;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // uploadTextBox
            // 
            this.uploadTextBox.Enabled = false;
            this.uploadTextBox.Location = new System.Drawing.Point(257, 140);
            this.uploadTextBox.Name = "uploadTextBox";
            this.uploadTextBox.Size = new System.Drawing.Size(755, 26);
            this.uploadTextBox.TabIndex = 20;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(163, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 22);
            this.label12.TabIndex = 22;
            this.label12.Text = "Select File :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHeader
            // 
            this.labelHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelHeader.BackColor = System.Drawing.Color.Snow;
            this.labelHeader.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeader.ForeColor = System.Drawing.Color.DarkRed;
            this.labelHeader.Location = new System.Drawing.Point(479, 34);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(815, 35);
            this.labelHeader.TabIndex = 19;
            this.labelHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::vzpapp.Properties.Resources.logo21;
            this.pictureBox1.Location = new System.Drawing.Point(1661, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 84);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox5.Controls.Add(this.groupBox3);
            this.groupBox5.Controls.Add(this.labelHeader);
            this.groupBox5.Controls.Add(this.pictureBox1);
            this.groupBox5.Controls.Add(this.groupBox4);
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Controls.Add(this.groupBox2);
            this.groupBox5.Location = new System.Drawing.Point(12, -12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1760, 1071);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // EcuEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1784, 1061);
            this.Controls.Add(this.groupBox5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EcuEntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ECU Function";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EcuEntryForm_FormClosing);
            this.Load += new System.EventHandler(this.EcuEntryForm_Load);
            this.Leave += new System.EventHandler(this.EcuEntryForm_Leave);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox yearComboBox;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox modelComboBox;
        private System.Windows.Forms.ComboBox buildComboBox;
        private System.Windows.Forms.ComboBox seriesComboBox;
        private System.Windows.Forms.ComboBox producerComboBox;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox buildgrpComboBox;
        private System.Windows.Forms.ComboBox ecuProducercComboBox;
        private System.Windows.Forms.ComboBox useComboBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox versiontextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox softwaretextBox;
        private System.Windows.Forms.TextBox ecutextBox;
        private System.Windows.Forms.TextBox pecutextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox vinTextBox;
        private System.Windows.Forms.TextBox licenseTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button processButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox uploadTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox dtctextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CusnametextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label labelStat;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button cancelButton;
    }
}