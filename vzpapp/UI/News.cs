﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using vzpapp.BAL;

namespace vzpapp.UI
{
    public partial class News : Form
    {
       
        MsgBox newsmsg;
        NewsDetails nd;
        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);

        public News()
        {
            InitializeComponent();
            SetAutoRotation(false);

        }







        private void News_Shown(object sender, EventArgs e)
        {

            try
            {
                if (Globals.news_stat == 0)
                {
                    newsmsg = new MsgBox("Getting Latest News for you", "news");
                    // newsmsg.showMsg();
                    newsmsg.ShowDialog();
                    Globals.news_stat = 1;

                }


                dataGridView1.DataSource = Globals.allNews.Select(x => new { x.id, x.create_at, x.subject, x.ustat }).ToList();
                dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[3].Visible = false;
                dataGridView1.EnableHeadersVisualStyles = false;

                DataGridViewColumn column_one = dataGridView1.Columns[1];
                column_one.Width = 200;
                DataGridViewColumn column_two = dataGridView1.Columns[2];
                column_two.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 14F, FontStyle.Bold);
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.RoyalBlue;
                dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

                // dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;

                dataGridView1.Columns[1].HeaderCell.Value = "Published";
                dataGridView1.Columns[2].HeaderCell.Value = "Subject";
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToOrderColumns = true;
                dataGridView1.ReadOnly = true;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = false;
                dataGridView1.AllowUserToResizeColumns = false;

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    int val = Int32.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                    if (val == 0)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.LightYellow;
                        dataGridView1.Rows[i].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Bold);

                    }
                    else
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        dataGridView1.Rows[i].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular);

                    }
                }
            }
            catch(Exception nex)
            {

                MessageBox.Show("News"+nex.Message);
            }

                // newsmsg.Close();
                //tmr.Stop();




            }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            int rowindex = dataGridView1.CurrentCell.RowIndex;
            //  int columnindex = dataGridView1.CurrentCell.ColumnIndex;

            String newsid = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
            nd = new NewsDetails();
            nd.ShowNewsDetails(newsid);
            nd.Show();
            this.Close();



        }

        private void News_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (new StackTrace().GetFrames().Any(x => x.GetMethod().Name == "Close"))
            {
                // MessageBox.Show("Closed by calling Close()");
            }
            else
            {
                Home hme = new Home();
                //this.Hide();
                hme.Show();
                //this.Close();

            }

        }

        
    }
}
