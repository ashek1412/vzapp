﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using vzpapp.BAL;

namespace vzpapp.UI
{
    public partial class Services : Form
    {
        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);
        
        String lockstat;
        private BackgroundWorker imageLoader;
        public Services()
        {
            InitializeComponent();
            SetAutoRotation(false);

            FormCollection openforms = Application.OpenForms;
         


            this.imageLoader = new BackgroundWorker();
            this.imageLoader.DoWork += HandleOnImageLoaderDoWork;
            this.imageLoader.RunWorkerCompleted += HandleImageLoaderOnRunWorkerCompleted;
            this.imageLoader.RunWorkerAsync();
            Thread.Sleep(5);
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;


        }

        private void HandleImageLoaderOnRunWorkerCompleted(Object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void HandleOnImageLoaderDoWork(Object sender, DoWorkEventArgs e)
        {
            pictureBox1.Click += new System.EventHandler(clickService);
            pictureBox2.Click += new System.EventHandler(clickService);
            pictureBox3.Click += new System.EventHandler(clickService);
            pictureBox4.Click += new System.EventHandler(clickService);
            pictureBox5.Click += new System.EventHandler(clickService);
            pictureBox6.Click += new System.EventHandler(clickService);
            pictureBox7.Click += new System.EventHandler(clickService);
            pictureBox8.Click += new System.EventHandler(clickService);
            pictureBox9.Click += new System.EventHandler(clickService);
            pictureBox10.Click += new System.EventHandler(clickService);
            pictureBox11.Click += new System.EventHandler(clickService);
            pictureBox12.Click += new System.EventHandler(clickService);
            pictureBox13.Click += new System.EventHandler(clickService);
            pictureBox14.Click += new System.EventHandler(clickService);
            pictureBox15.Click += new System.EventHandler(clickService);
            pictureBox16.Click += new System.EventHandler(clickService);
            pictureBox17.Click += new System.EventHandler(clickService);
            pictureBox18.Click += new System.EventHandler(clickService);
            pictureBox19.Click += new System.EventHandler(clickService);
            pictureBox20.Click += new System.EventHandler(clickService);
            pictureBox21.Click += new System.EventHandler(clickService);
            pictureBox22.Click += new System.EventHandler(clickService);
            pictureBox23.Click += new System.EventHandler(clickService);
            pictureBox24.Click += new System.EventHandler(clickService);
            pictureBox25.Click += new System.EventHandler(clickService);
            pictureBox26.Click += new System.EventHandler(clickService);
            pictureBox27.Click += new System.EventHandler(clickService);
            pictureBox28.Click += new System.EventHandler(clickService);
            pictureBox29.Click += new System.EventHandler(clickService);
            pictureBox30.Click += new System.EventHandler(clickService);
            pictureBox31.Click += new System.EventHandler(clickService);
            pictureBox32.Click += new System.EventHandler(clickService);
            pictureBox33.Click += new System.EventHandler(clickService);
            pictureBox34.Click += new System.EventHandler(clickService);
            pictureBox35.Click += new System.EventHandler(clickService);
            pictureBox36.Click += new System.EventHandler(clickService);
            pictureBox37.Click += new System.EventHandler(clickService);
            pictureBox38.Click += new System.EventHandler(clickService);
            pictureBox39.Click += new System.EventHandler(clickService);
            pictureBox40.Click += new System.EventHandler(clickService);
            pictureBox41.Click += new System.EventHandler(clickService);
            pictureBox42.Click += new System.EventHandler(clickService);
            pictureBox43.Click += new System.EventHandler(clickService);
            pictureBox44.Click += new System.EventHandler(clickService);
            pictureBox45.Click += new System.EventHandler(clickService);
            pictureBox46.Click += new System.EventHandler(clickService);
            pictureBox47.Click += new System.EventHandler(clickService);
            pictureBox48.Click += new System.EventHandler(clickService);
            pictureBox49.Click += new System.EventHandler(clickService);
            pictureBox50.Click += new System.EventHandler(clickService);
            pictureBox51.Click += new System.EventHandler(clickService);
            pictureBox52.Click += new System.EventHandler(clickService);
            pictureBox53.Click += new System.EventHandler(clickService);
            pictureBox54.Click += new System.EventHandler(clickService);
            pictureBox55.Click += new System.EventHandler(clickService);





            foreach (var s in Globals.user_service)
            {
                lockstat = "lock" + s.fid;
                //Label lb = (Label)FindControl(lockstat);
                //string labelName = "lbl_text";
                Label lbl_lock = this.Controls.Find(lockstat, true).FirstOrDefault() as Label;
                lbl_lock.Visible = false;

            }

        }

        public void clickService(object sender, EventArgs e)
        {



            String resultString = Regex.Match((sender as PictureBox).Name, @"\d+").Value;
            int ecuLimit = int.Parse(BAL.Globals.currentUser.ecufile_limit);

            if (Globals.user_service.Exists(x => x.fid == resultString))
            {
                //  String sName = Globals.user_service.Where(x => x.fid == resultString).Select(x => x.fname).ToString();
                string sName = Globals.user_service.First(s => s.fid == resultString).fname;
                EcuEntryForm srvhm = new EcuEntryForm(sName);
                //this.Hide();
                
                srvhm.Show();
                
                this.Close();

            }
            else
            {
                MessageBox.Show("No access, contact technology");

            }


        }



        private void Services_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Services_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (new StackTrace().GetFrames().Any(x => x.GetMethod().Name == "Close"))
            {
                // MessageBox.Show("Closed by calling Close()");
            }
            else
            {
                Home hme = new Home();
                //this.Hide();
                hme.Show();
                Thread.Sleep(1000);

            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tabPage2");
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tabPage1");
        }

        private void buttonNext1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tabPage3");
        }

        private void prefbutton3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab("tabPage2");
        }
    }
}
