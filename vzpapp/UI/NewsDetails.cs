﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vzpapp.BAL;

namespace vzpapp.UI
{
    public partial class NewsDetails : Form
    {
        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);
        public NewsDetails()
        {
            InitializeComponent();
            SetAutoRotation(false);
        }


        public void ShowNewsDetails(string nid)
        {
            try
            {

                var htclstr = Globals.allNews.First(s => s.id == nid).description;

                DNews dn = new DNews();
                dn.SetNewsViewed(BAL.Globals.user_name, nid);
                webBrowser1.DocumentText = htclstr.ToString();
            }
            catch(Exception en)
            {


            }


        }

 

        private void NewsDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            News f2 = new News();
            f2.Show();
        }
    }
}
