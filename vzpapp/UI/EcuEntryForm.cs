﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using vzpapp.BAL;
using Timer = System.Windows.Forms.Timer;

namespace vzpapp.UI
{
    public partial class EcuEntryForm : Form
    {
        List<Vehicles> vl;
        List<Ecu> el;
        MsgBox newsmsg;
        Vehicles v;
        Ecu ec;
        EcuFIle newEcu;
        Timer tmr;
        int tcount, chestat;
        MemoryStream responseStream;
        BackgroundWorker upload_worker;
        BackgroundWorker download_worker;
        BackgroundWorker check_worker;
        [DllImport("user32.dll", EntryPoint = "#2507")]
        extern static bool SetAutoRotation(bool bEnable);

        public EcuEntryForm(string sname)
        {
            InitializeComponent();
            labelHeader.Text = sname;
            this.MaximizeBox = false;

           this.Size= new Size(1920, 1200);
            SetAutoRotation(false);



            foreach (Control control in groupBox2.Controls)
            {
                // Check if the object is indeed what we need
               

                if (control is TextBox)
                {
                    TextBox textBox = control as TextBox;
                    textBox.Leave += new EventHandler(text_Leave);
                }

                // Do your thing with the textboxes
            }
            foreach (Control control in groupBox1.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = control as TextBox;
                    textBox.Leave += new EventHandler(text_Leave);
                }

                else if (control is ComboBox)
                {
                    ComboBox cm = control as ComboBox;
                    cm.Leave += new EventHandler(Combo_Leave);
                }


            }

            foreach (Control control in groupBox3.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = control as TextBox;
                    textBox.Leave += new EventHandler(text_Leave);
                }

                else if (control is ComboBox)
                {
                    ComboBox cm = control as ComboBox;
                    cm.Leave += new EventHandler(Combo_Leave);
                }

                // Do your thing with the textboxes
            }

            //v.GetDistinctProducer("PKW");
           
        }

      
        private void text_Leave(object sender, EventArgs e)
        {
            TextBox text = (TextBox)sender;
            //MessageBox.Show(text.Text);
            this.WindowState = FormWindowState.Maximized;
        }

        private void Combo_Leave(object sender, EventArgs e)
        {
            ComboBox cbm = (ComboBox)sender;
           //MessageBox.Show(cbm.Text);
            this.WindowState = FormWindowState.Maximized;
        }



        private void EcuEntryForm_Leave(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }


        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            producerComboBox.DataSource = null;
            seriesComboBox.DataSource = null;
            buildComboBox.DataSource = null;
            modelComboBox.DataSource = null;

            var producers = v.GetVehicleProducer(typeComboBox.Text);
            producerComboBox.DataSource = producers;
            //producerComboBox
        }

        private void producerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            seriesComboBox.DataSource = null;
            buildComboBox.DataSource = null;
            modelComboBox.DataSource = null;
            var series = v.GetVehicleSeries(typeComboBox.Text, producerComboBox.Text);
            seriesComboBox.DataSource = series;
        }

        private void seriesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            buildComboBox.DataSource = null;
            modelComboBox.DataSource = null;

            var build = v.GetVehiclebuild(typeComboBox.Text, producerComboBox.Text, seriesComboBox.Text);
            //if (build != null && !(build.Count==1 && build[0].Length==0))
            if (build != null && !(build.Count == 1 && string.IsNullOrEmpty(build[0])))
            {
                buildComboBox.DataSource = build;

            }
            else
            {
                var model = v.GetVehicleModel(typeComboBox.Text, producerComboBox.Text, seriesComboBox.Text);
                modelComboBox.DataSource = model;

            }

        }

        private void buildComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            modelComboBox.DataSource = null;
            var model = v.GetVehicleModelBuild(typeComboBox.Text, producerComboBox.Text, seriesComboBox.Text, buildComboBox.Text);
            modelComboBox.DataSource = model;

        }

        private void ecuProducercComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            buildgrpComboBox.DataSource = null;
            var buildgrp = ec.GetEcuBuild(ecuProducercComboBox.Text, useComboBox.Text);
            buildgrpComboBox.DataSource = buildgrp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Multiselect = false;
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse ECU Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "dtf",
                Filter = "ECU File (*.dtf, *.bin, *.mpc, *.zip, *.bdc , *.slave, *.rar, *.mmf,*.s19,*.hex )|*.dtf; *.bin; *.mpc; *.zip; *.bdc; *.slave; *.rar; *.mmf; *.s19;*.hex",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FileName.ToString().Contains(" "))
                {

                    MessageBox.Show("File Name can not have Space!!");
                }
                else
                {
                    uploadTextBox.Text = openFileDialog1.FileName;
                    processButton.Enabled = true;
                    processButton.BackColor = Color.LightPink;
                }
            }
        }

        private void processButton_Click(object sender, EventArgs e)
        {
            TimeSpan stime = TimeSpan.Parse(BAL.Globals.currentUser.service_stime);
            TimeSpan etime = TimeSpan.Parse(BAL.Globals.currentUser.service_etime);
            TimeSpan srvtime = TimeSpan.Parse(BAL.Globals.currentUser.server_time);
            DateTime lastEcufileTime = DateTime.Now;

            int ecuLimit =int.Parse(BAL.Globals.currentUser.ecufile_limit);
            int serviceCount=0;
            Credstat cr = Globals.currentUser.GetTodayServiceCount();
            int lastMin = BAL.Globals.currentUser.ecu_delay_time;

            if (!String.IsNullOrEmpty(cr.stat))
                serviceCount = Int32.Parse(cr.stat);

            if (!String.IsNullOrEmpty(cr.lastServiceDate))
            {
                lastEcufileTime = DateTime.Parse(cr.lastServiceDate);
                lastMin = (int)DateTime.Now.Subtract(lastEcufileTime).TotalMinutes;
            }


            if (lastMin < BAL.Globals.currentUser.ecu_delay_time)
            {

                MessageBox.Show("Please process File after some time !");

            }
            else if (serviceCount > ecuLimit)
            {

                MessageBox.Show("Number of ECU File Submitted Exceeds Maximum Limit for Today !");

            }
            else if (BAL.Globals.currentUser.service_status.Equals("0"))
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else if (etime <= srvtime)
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else if (stime >= srvtime)
            {

                MessageBox.Show("Server Maintenance is Underway !!");
            }
            else
            {


                Boolean isValidInput = Validateform();
                if (isValidInput == true)
                {

                    if (MessageBox.Show("Do you want to Start Processing ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        //do something if YES
                        try
                        {

                            if (BAL.Globals.CheckInternet())
                            {

                                newEcu = new EcuFIle();
                                newEcu.ecu_file_dl_stat = 0;
                                processButton.Enabled = false;
                                processButton.BackColor = Color.Gold;

                                newEcu.cus_name = CusnametextBox.Text.Trim();
                                newEcu.cus_license = licenseTextBox.Text.Trim();
                                newEcu.cus_vin = vinTextBox.Text.Trim();
                                newEcu.cus_dtc = dtctextBox.Text.Trim();

                                newEcu.v_type = typeComboBox.Text.Trim();
                                newEcu.v_producer = producerComboBox.Text.Trim();
                                newEcu.v_series = seriesComboBox.Text.Trim();
                                newEcu.v_build = buildComboBox.Text.Trim();
                                newEcu.v_model = modelComboBox.Text.Trim();
                                newEcu.v_year = yearComboBox.Text.Trim();

                                newEcu.ecu_use = useComboBox.Text.Trim();
                                newEcu.ecu_producer =ecuProducercComboBox.Text.Trim();
                                newEcu.ecu_build = buildgrpComboBox.Text.Trim();
                                newEcu.ecu_nr_prd = pecutextBox.Text.Trim();
                                newEcu.ecu_nr_ecu = ecutextBox.Text.Trim();
                                newEcu.ecu_software = softwaretextBox.Text.Trim();
                                newEcu.ecu_version = versiontextBox.Text.Trim();
                                newEcu.ecu_file_type = labelHeader.Text.Trim();
                                newEcu.ecu_file_path = uploadTextBox.Text.Trim();
                                newEcu.ecu_file_save_id = 0;
                                labelStat.Text = "Processing...";

                                upload_worker = new BackgroundWorker();
                                upload_worker.DoWork += new DoWorkEventHandler(upload_worker_DoWork);
                                upload_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(upload_worker_RunWorkerCompleted);
                                upload_worker.RunWorkerAsync();


                                check_worker = new BackgroundWorker();
                                check_worker.DoWork += new DoWorkEventHandler(check_worker_DoWork);
                                upload_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(check_worker_RunWorkerCompleted);
                                // upload_worker.RunWorkerAsync();

                                download_worker = new BackgroundWorker();
                                download_worker.DoWork += new DoWorkEventHandler(download_worker_DoWork);
                                download_worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(download_worker_RunWorkerCompleted);

                                tmr = new Timer();

                                tmr.Interval = 500;

                                progressBar1.Maximum = 100;
                                tcount = 0;
                                tmr.Start();

                                tmr.Tick += tmr_Tick;
                            }
                            else
                            {

                                MessageBox.Show("Unable to Process file!!");

                            }

                        }
                        catch
                        {
                            MessageBox.Show("Unable to Process file!!");

                        }
                    }
                    else
                    {
                        //do something if NO
                    }
                }
            }
        }


        public bool Validateform()
        {
            Boolean b = true;



            if (string.IsNullOrEmpty(producerComboBox.Text))
            {
                producerComboBox.Focus();
                errorProvider1.SetError(producerComboBox, "Please Select Vehicle Producer");
                b = false;
            }
            else
            {
                errorProvider1.SetError(producerComboBox, null);
            }


            if (string.IsNullOrEmpty(seriesComboBox.Text))
            {
                seriesComboBox.Focus();
                errorProvider1.SetError(seriesComboBox, "Please Select Vehicle Series");
                b = false;
            }
            else
            {
                errorProvider1.SetError(seriesComboBox, null);
            }



            if (string.IsNullOrEmpty(modelComboBox.Text))
            {
                modelComboBox.Focus();
                errorProvider1.SetError(modelComboBox, "Please Select Vehicle Model");
                b = false;
            }
            else
            {
                errorProvider1.SetError(modelComboBox, null);
            }

            if (string.IsNullOrEmpty(yearComboBox.Text))
            {
                yearComboBox.Focus();
                errorProvider1.SetError(yearComboBox, "Please Select Vehicle Model Year");
                b = false;
            }
            else
            {
                errorProvider1.SetError(yearComboBox, null);
            }

            if (string.IsNullOrEmpty(ecuProducercComboBox.Text))
            {
                yearComboBox.Focus();
                errorProvider1.SetError(ecuProducercComboBox, "Please Select ECU Producer");
                b = false;
            }
            else
            {
                errorProvider1.SetError(ecuProducercComboBox, null);
            }

            if (string.IsNullOrEmpty(buildgrpComboBox.Text))
            {
                buildgrpComboBox.Focus();
                errorProvider1.SetError(buildgrpComboBox, "Please Select ECU Build");
                b = false;
            }
            else
            {
                errorProvider1.SetError(buildgrpComboBox, null);
            }


            return b;


        }






        private void upload_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            newEcu.ecu_file = newEcu.PrepareEcuFile(uploadTextBox.Text);
            newEcu.ecu_file_save_id = newEcu.SaveEcuFile();
     
        }

        private void upload_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        private void check_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            
           while(chestat==0)
           {
                Thread.Sleep(5000);
                chestat = newEcu.CheckEcuFile();
                if(tcount>=6400)
                {
                    chestat = 0;
                    newEcu.CancelEcuFile();
                    cancelButton.Enabled = false;
                    break;
                }
            }           
        }

        private void check_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
     
        }
        


        private void download_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //int dl_stat = download();
            int dl_stat = DownLoadHTTP();
            if (dl_stat == 1)
            {
                newEcu.ecu_file_dl_stat = 2;
            }
            else
            {
                newEcu.ecu_file_dl_stat = 3;
            }

        }

        private void download_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
     
        }


        void tmr_Tick(object sender, EventArgs e)
        {

            if (progressBar1.Value == 100)
                progressBar1.Value = 1;


            progressBar1.Value++;
            if (tcount < 64)
            {
                if ((tcount % 2) == 0 && newEcu.ecu_file_save_id > 0)
                {
                    if (check_worker.IsBusy == false)
                        check_worker.RunWorkerAsync();
                    if (chestat == 1)
                    {


                        if (newEcu.ecu_file_dl_stat == 0)
                        {
                            newEcu.ecu_file_dl_stat = 1;
                            download_worker.RunWorkerAsync();

                        }
                        else if (newEcu.ecu_file_dl_stat == 2)
                        {


                            tmr.Stop();
                            progressBar1.Value = 100;
                            saveButton.Visible = true;
                            labelStat.Text = "File Process Completed";
                            MessageBox.Show(this, "File Process Complete !!");
                            processButton.Enabled = false;
                            processButton.BackColor = Color.WhiteSmoke;
                            button1.Enabled = false;
                            saveButton.Visible = true;

                        }
                        else if (newEcu.ecu_file_dl_stat == 3)
                        {
                            tmr.Stop();
                            progressBar1.Value = 100;
                            //labelStat.Text = "Our Tool Cannot Process your file.";
                            labelStat.Text = "I am sorry that there is no solution for our device. Please contact the technical staff for assistance.";
                            processButton.Enabled = false;
                            processButton.BackColor = Color.WhiteSmoke;
                            button1.Enabled = false;

                        }
                    }
                    else if (chestat == 2)
                    {
                        tmr.Stop();
                        progressBar1.Value = 100;
                        labelStat.Text = "Cannot process your file. Please check your network and start again.";
                        processButton.Enabled = false;
                        processButton.BackColor = Color.WhiteSmoke;
                        button1.Enabled = false;

                    }

                }
                else if(newEcu.ecu_file_save_id==-1)
                {
                    tmr.Stop();
                    progressBar1.Value = 100;
                    labelStat.Text = "Cannot process your file. Please check your network and start again";
                    processButton.Enabled = false;
                    processButton.BackColor = Color.WhiteSmoke;
                    button1.Enabled = false;

                }
                    

            }
            else
            {
                labelStat.Text = "Cannot process your file. Please check your network and start again";
                progressBar1.Value = 100;
                tmr.Stop();
                processButton.Enabled = false;
                processButton.BackColor = Color.WhiteSmoke;
                button1.Enabled = false;
                newEcu.CancelEcuFile();
                cancelButton.Enabled = false;

            }

            tcount++;

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {

                string vol = BAL.Globals.usbDrive;
                string FileName = newEcu.SetDownloadFileName();
                FileName = vol + "\\" + FileName;
                if (BAL.Globals.currentUser.GetUsbStatus()==1)
                {

                    using (var fileStream = new FileStream(@"" + FileName, FileMode.Create, FileAccess.Write))
                    {                        
                        responseStream.Position = 0;
                        responseStream.CopyTo(fileStream);                                      

                    }

                    if (!File.Exists(@"" + FileName))
                    {
                        labelStat.Text = "File Cannot be Saved";
                        MessageBox.Show(this,"File Cannot be Saved");
                    }
                    else
                    {
                        MessageBox.Show(this,"File Saved to USB Drive");
                        string argument = "/select, \"" + FileName + "\"";
                        // System.Diagnostics.Process.Start("explorer.exe", argument);
                        ViewFile vm = new ViewFile();
                        //vm.BringToFront();
                        vm.ShowDialog(this);
                        vm.BringToFront();


                    }
                }
                else
                {
                    MessageBox.Show(this,"USB Drive not found");
                }                
            }
            catch (Exception ex)
            {
                labelStat.Text = "File Cannot be Saved";
                MessageBox.Show(this,"File Cannot be Saved");
            }
        }

        private void EcuEntryForm_Load(object sender, EventArgs e)
        {
            try
            {
                newsmsg = new MsgBox("Laoding all ECU data", "ecu");
                newsmsg.ShowDialog();

                v = new Vehicles();
                if (BAL.Globals.allVehilces == null || BAL.Globals.allECU == null)
                {
                    MessageBox.Show("Unable to Load Data !!");
                    this.Close();


                }
                else
                {
                    vl = BAL.Globals.allVehilces;
                    var vtyp = vl.OrderBy(x => x.type).Select(x => x.type).Distinct().ToList();
                    typeComboBox.DataSource = vtyp;
                    typeComboBox.SelectedIndex = 5;
                    yearComboBox.DataSource = v.GetVehicleYear();
                    yearComboBox.SelectedIndex = 20;


                    ec = new Ecu();
                    el = BAL.Globals.allECU;
                    useComboBox.DataSource = ec.GetEcuUse();
                    useComboBox.SelectedIndex = 2;
                    var ecuProd = el.OrderBy(x => x.producer).Where(x => x.producer.Length > 1).Select(x => x.producer).Distinct().ToList();
                    ecuProducercComboBox.DataSource = ecuProd;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to Load Data !!");
                this.Close();

            }
        }

        private void EcuEntryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (tmr != null && tmr.Enabled)
            {
                e.Cancel = true;
                MessageBox.Show("Process is Running. Do not close");

            }
            else            
            {
                Services sv1 = new Services();               
                sv1.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (tmr != null && tmr.Enabled)
            {
                MessageBox.Show("Process is Running. Do not close");
            }
            else
            {
                this.Close();
            }
        }

      

        public int download()
        {
            if (BAL.Globals.CheckInternet() == false)
                return 0;
            try
            {

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://35.213.169.129:21/desktop_files/" + newEcu.ecu_dl_file);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
               
                request.Credentials = new NetworkCredential("dev@support.vz-performance.com", "wjq163558");
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                responseStream = new MemoryStream();
                response.GetResponseStream().CopyTo(responseStream);
                response.Close();
                return 1;

            }
            catch (WebException e)
            {
                String status = ((FtpWebResponse)e.Response).StatusDescription;
                // MessageBox.Show(status);
                Globals.WriteToFile(Globals.ExceptionInfo(e));
                return 0;
            }


        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

            if (newEcu!=null && newEcu.ecu_file_save_id > 0)
            {
                if (MessageBox.Show("Do you want to Cancel Process ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //do something if YES
                    try
                    {
                        labelStat.Text = "Process Cancelled";
                        progressBar1.Value = 100;
                        tmr.Stop();
                        processButton.Enabled = false;
                        processButton.BackColor = Color.WhiteSmoke;
                        button1.Enabled = false;
                        newEcu.CancelEcuFileByUesr();

                        cancelButton.Enabled = false;
                        MessageBox.Show(this, "Process cancelled.Thank you for using our tool."+ Environment.NewLine + " Please contact our online support for any question");

                    }
                    catch (Exception ex)
                    {
                        //labelStat.Text = "File Cannot be Saved";
                        MessageBox.Show(this, "Cannot Cancel");
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Process Cannot be canceled !!!");

            }

        }

        public int DownLoadHTTP()
        {
            if (BAL.Globals.CheckInternet() == false)
                return 0;
            try
            {
              

                var client = new RestClient("https://www.ecu-manager.com/magic/api/download_file");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("fname", newEcu.ecu_dl_file);

                var encoding = ASCIIEncoding.ASCII;


                
                responseStream = new MemoryStream();
                Byte[]  response = client.DownloadData(request);
                //response.GetResponseStream().CopyTo(responseStream);
                //responseStream.Write(response);
                if (response.Length > 0)
                {
                    responseStream = new MemoryStream(response);
                   
                    return 1;
                }
                else
                {
                    return 0;

                }



                   

            }
            catch (Exception we)
            {
                //String status = ((FtpWebResponse)e.Response).StatusDescription;
                // MessageBox.Show(status);
                //Globals.WriteToFile(Globals.ExceptionInfo(e));
                return 0;
            }


        }
    }
}
