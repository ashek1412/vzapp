﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using vzpapp.BAL;
using vzpapp.UI;
using Timer = System.Windows.Forms.Timer;

namespace vzpapp
{
    public partial class SplashForm : Form
    {
        //Timer tmr;
      
        Reg userReg;
        Credential crd;
        Timer timer1;



        public SplashForm()
        {
            InitializeComponent();
            crd = new Credential();
            userReg = new Reg(); 
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            

            // Globals.bgwDriveDetector = new BackgroundWorker();
            //  bgwDriveDetector = new BackgroundWorker();
            // Globals.bgwDriveDetector.DoWork += new DoWorkEventHandler(bgwDriveDetector_DoWork);
            // Globals.bgwDriveDetector.WorkerSupportsCancellation = true;
            label1.Text = "Version : "+Application.ProductVersion;

            //userReg = crd.GetUserNewMachineID();
            //if (userReg == null || String.IsNullOrEmpty(userReg.email))
            //    userReg = crd.GetUserMachineID();

            //Verify_device();



        }





        private  void timer1_Tick(object sender, EventArgs e)
        {
            int isUsb = 0;
            isUsb = Globals.VerifyUsbDvice();
            if(isUsb==0)
            {
                Globals.usbErrCnt++;

                timer1.Stop();
                if (Globals.usbErrCnt>10)
                {

                    MessageBox.Show("USB device detection failed too many times !!! Contact Service", "Error");
                    timer1.Stop();
                    this.Close();

                }
                if (MessageBox.Show(this,"USB Device not found. Please insert Genuine USB device !", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                {
                    while(Globals.VerifyUsbDvice()==0)
                    {
                        if (MessageBox.Show(this,"USB Device not found. Please insert Genuine USB device !", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                        {

                        }
                        else
                        {
                          
                            this.Close();

                          
                            break;
                            

                        }

                    }


                    timer1.Start();

                }
                else
                {
                    timer1.Stop();
                    this.Close();
                }


            }

           

        }







        private void SplashForm_Shown(object sender, EventArgs e)
        {
            userReg = crd.GetUserNewMachineID();
            if (userReg == null || String.IsNullOrEmpty(userReg.email))
                userReg = crd.GetUserMachineID();

            Verify_device();
        }

        private void Verify_device()
        {
                      
            int usbstat = 0;


            if (userReg == null || String.IsNullOrEmpty(userReg.email))
            {
               
                if (MessageBox.Show("Do you want to Register this device ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Registration rg = new Registration();
                    rg.Show();
                    this.Hide();
                }
                else
                {
                    Application.Exit();

                }

            }
            else if (String.IsNullOrEmpty(userReg.device_pkey) || String.IsNullOrEmpty(userReg.device_id))
            {
                DialogResult dialog = MessageBox.Show("USB Device information missing", "Error", MessageBoxButtons.OK);
                if (dialog == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
            else
            {

                usbstat = userReg.GetUsbStatus();

                if (usbstat != 1)
                {
                    

                    Globals.usbErrCnt++;
                    if (Globals.usbErrCnt > 10)
                    {

                        MessageBox.Show("USB device detection failed too many times !!! Contact Service", "Error");

                        Application.Exit();

                    }

                    if (MessageBox.Show("USB Device not found. Please insert correct USB device !", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                    {
                        Verify_device();
                        // usbstat = getreg.GetUsbDrives(userReg.device_pkey, userReg.device_id);
                        //SplashForm sp = new SplashForm();
                        //this.Dispose();
                        //sp.Show();

                    }
                   else
                    {
                       // LockWorkstation.LockWorkStation();
                        Application.Exit();
                    }
                }
                else
                {
                    Credential cl = new Credential();
                    userReg.app_ver= Application.ProductVersion;
                    Globals.currentUser = userReg;
                    BAL.Globals.user_name = userReg.uname;
                    BAL.Globals.user_sl = userReg.sl_num;
                    BAL.Globals.exp_date = userReg.exp_date;
                    BAL.Globals.user_news = userReg.GetUserNewsCount();

                    Globals.user_service = cl.GetUserServices(Globals.currentUser.uname);

                    Home hm = new Home();
                    hm.Show();
                    this.Hide();
                    timer1 = new Timer();
                    timer1.Tick += new EventHandler(timer1_Tick);
                    timer1.Interval = 5000; // in miliseconds
                    timer1.Start();



                }
            }

        }
    }
}



        
    

